#include <iostream>
#include "filemanager.h"
#include "invertedindex.h"
#include "File/file.h"
#include <ctime>
#include "config.h"
#include "query.h"
#include "models.h"
#include "adarankalgorithm.h"

#include "qrelsmanager.h"

#include <assert.h>
#include <stdlib.h>
#include <algorithm>
#include <string>
#include "other/filesystem.h"

#ifndef PROJECT_PATH
#define PROJECT_PATH ""
#endif

using namespace std;

// random generator function:
int myrandom (int i) ;
int myrandom (int i) { return rand()%(unsigned int)i;}

int main()
{
    // TODO: Ask where are the Collection files, by default "Collection/"

    cout<<"-------------------------------------\n";
    cout<<" IR System \n";
    cout<<"-------------------------------------\n";
    cout<<" Team:\n";
    cout<<"   * Camelia Simoiu         10664122 \n";
    cout<<"   * Hongpei Xu             10629025 \n";
    cout<<"   * Andreu Gonzalez Llinas 10656677 \n";
    cout<<"-------------------------------------\n";

#if __cplusplus > 199711L
    cout<< "Compiler supports c++11"<<endl;

    if( sizeof(void*) == 8)
    {
        cout<<"Program running in 64-bits mode"<<endl;
    }else{
        cout<<"Program running in 32-bits mode"<<endl;
    }

#endif

    FileManager *fileM = FileManager::getInstance();
    InvertedIndex *invIndM = InvertedIndex::getInstance();
     QrelsManager *qM = QrelsManager::getInstance();


// Control Menu

    bool FullColl;
    bool WITHSTEMING;
    bool init = false;


    string answer("");
    cout<< " This menu will appear after complete all"<<endl;
    cout<< " Do you want to exit ? ( y: yes, n: no, other: no)"<<endl;
    getline(cin, answer);
    if( answer == "Y" || answer == "y" )
    {
        cout<<"------------------------------------------------\n";
        if( init )
        {   // release memory
            cout<<"Deleting Allocated Memory"<<endl;
            delete fileM;
            delete invIndM;
            delete qM;
        }
        cout <<"Done - End of Program!"<<endl;
        return 0;
    }

askColl:
    cout<< " First Choose the collection ( 0: Complete, 1: Small )"<<endl;
    getline(cin, answer);
    if( answer == "0")
    {
        if( init && !FullColl )
        {
            init = false;
            cout<<"Deleting Allocated Memory"<<endl;
            delete fileM;
            delete invIndM;
            delete qM;
        }
        FullColl = true;
    }
    else if( answer == "1")
    {
        if( init && FullColl )
        {
            init = false;
            cout<<"Deleting Allocated Memory"<<endl;
            delete fileM;
            delete invIndM;
            delete qM;
        }
        FullColl = false;
    }
    else{
        cout<<" Error values, only 0 or 1, ( 0: Complete, 1: Small )"<<endl;
        goto askColl;
    }

    cout<< " Do you want to activate steaming: ( y: yes, n: no, other: yes)"<<endl;
    getline(cin, answer);
    if( answer == "N" || answer == "n" )
    {
        if(init && WITHSTEMING)
        {
            init = false;
            cout<<"Deleting Allocated Memory"<<endl;
            delete fileM;
            delete invIndM;
            delete qM;
        }
        WITHSTEMING = false;
    }else{
        if( init && !WITHSTEMING)
        {
            init = false;
            cout<<"Deleting Allocated Memory"<<endl;
            delete fileM;
            delete invIndM;
            delete qM;
        }
        WITHSTEMING = true;
    }

askModel:
    bool TF_IDF = false;
    bool BM25 = false;
    bool ADARANK = false;

    cout<< " Which model do you want to run? ( 0: TF-IDF, 1: BM-25, 2: AdaRank )"<<endl;
    getline(cin, answer);
    if( answer == "0" )
        TF_IDF = true;
    else if( answer == "1")
        BM25 = true;
    else if( answer == "2")
        ADARANK = true;
    else
        cout<< "  Error selecting the model"<<endl;
    cout<< " Do you want to select other model ? ( y: yes, n: no, other: no)"<<endl;
    getline(cin, answer);
    if( answer == "y" || answer == "Y")
        goto askModel;


    string R_FOLDER("Results_SmallColl_NoPorters/");
    if( FullColl && WITHSTEMING)
        R_FOLDER = "Results_FullColl_All/";
    else if( FullColl )
        R_FOLDER = "Results_FullColl_NoPorters/";
    else if( WITHSTEMING)
        R_FOLDER = "Results_SmallColl_All/";

    // CONTROL PANEL
//#define FullColl 1
//#define WITHSTEMING 0

//#define TF_IDF 0
//#define BM25 0
//#define ADARANK 1

#define TUNNEBM25 0
#define SHUFFLE_Y 0
#define SHUFFLE_TEST 0

    // ===================


//#if FullColl && WITHSTEMING
//    #define R_FOLDER "Results_FullColl_All/"
//#elif FullColl
//    #define R_FOLDER "Results_FullColl_NoPorters/"
//#elif WITHSTEMING
//    #define R_FOLDER "Results_SmallColl_All/"
//#else
//    #define R_FOLDER "Results_SmallColl_NoPorters/"
//#endif

DIR *dir = opendir(string(string(PROJECT_PATH)+string(R_FOLDER)).c_str() );
if(dir == NULL)
{
    //cout<<"directory not exist creating"<<endl;
    Create_Directory( string(string(PROJECT_PATH)+string(R_FOLDER)).c_str() );
}




if( !init )
{

//#if WITHSTEMING
    if( WITHSTEMING)
        fileM->addPreprocessor( STEMING | COUNTER );// | EMAIL | DOMAIN
//#else
    else
        fileM->addPreprocessor( COUNTER );
//#endif

    cout<<"Reading Collection: "; cout.flush();

     clock_t time = clock();

//#if FullColl && WITHSTEMING
     if( FullColl && WITHSTEMING )
     {
     // FULL COLLECTION with STEMING
        if( fileM->initWithBagOfWords(string(PROJECT_PATH) + string("Collection/BagsOfWords.txt") ))
        {
            clock_t time2 = clock();
            cout<< "(Read: "<<fileM->getNumberOfDocumentsReaded()<<") (Memory: "<<fileM->getSize()<<") : ";
            cout<< ((float)(time2-time))/CLOCKS_PER_SEC<< " seconds"<<endl;
        }else{
            cout<< "Error in reading the files"<<endl;
        }

//#elif FullColl
     // FULL COLLECTION without STEMING
     }
     else if( FullColl )
     {   if( fileM->initFullCollection( string(PROJECT_PATH) + string("corpus.body/") ))
        {
            clock_t time2 = clock();
            cout<< "(Read: "<<fileM->getNumberOfDocumentsReaded()<<") (Memory: "<<fileM->getSize()<<") : ";
            cout<< ((float)(time2-time))/CLOCKS_PER_SEC<< " seconds"<<endl;
        }else{
            cout<< "Error in reading the files"<<endl;
        }
//#else
     // SMALL COLLECTION
     }else{
         if( fileM->init( string( PROJECT_PATH) + string("CollectionOLD/") ) )
         {
             clock_t time2 = clock();
             cout<< "(Read: "<<fileM->getNumberOfDocumentsReaded()<<") (Memory: "<<fileM->getSize()<<") : ";
             cout<< ((float)(time2-time))/CLOCKS_PER_SEC<< " seconds"<<endl;
         }else{
             cout<< "Error in reading the files"<<endl;
         }
     }
//#endif

//    cout<<"Writing BagOfWords: "; cout.flush();
//    time = clock();
//    fileM->writeFiles( string(PROJECT_PATH) + string("log/") );
//    cout<< ((float)(clock()-time))/CLOCKS_PER_SEC<< "seconds"<<endl;

    cout<<"Creating Inverted Index: "; cout.flush();
    time = clock();
    fileM->initInvertedIndex();
    cout << ((float)(clock()-time))/CLOCKS_PER_SEC<< "seconds"<<endl;

//    cout<<"Writing Inverted Index: "; cout.flush();
//    time = clock();
//    invIndM->writeFile(string(PROJECT_PATH) + string("log/invertedIndex.txt") );
//    cout << ((float)(clock()-time))/CLOCKS_PER_SEC<< "seconds"<<endl;


    cout<<"------------------------------------------------\n";
    cout<<" Summary of the collection and querys loaded \n";
    cout<<" – total number of unique tokens: "<< invIndM->getTotalWords() <<endl;
    fileM->ShowEndsPreprocessors();

    qM->init(string(PROJECT_PATH) + string("Collection/document-qrels.txt"),string(PROJECT_PATH) + string("Collection/topics-original.txt"));

    cout<<"------------------------------------------------\n";
    cout<<" Welcome to our IR-System!"<<endl;

    init = true;
}

//    bool exit = false;
//    string command;
//    while( !exit )
//    {
//        cout<<"testMenu Type: Exit to exit the menu"<<endl;

        //======
        // test bm25:

    vector<resultList> rank;
    //=================================================================================================
    // TF-IDF
    //=================================================================================================
if(TF_IDF)
{
    cout<<"testing TF-IDF"; cout.flush();
    for( size_t queryID = 1; queryID<=50; ++queryID)
    {
        //cout<<" tfidf "<< queryID; cout.flush();
        Query *q = qM->getQuery(queryID);
        vector<unsigned int> *Y = qM->getJudgements(queryID);
        if( q == NULL || Y == NULL){

            // do not go to the next line (rank.erase())
            // but skip to next iterator i
            continue;
        }
        assert( q != NULL);

        tfidf(*q,rank);

        //printResults(queryID, "tfidf", rank);
        saveResults(string(PROJECT_PATH)+string(R_FOLDER)+string("TFIDF/"), queryID, "tfidf", rank);

        precisionRecall(string(PROJECT_PATH)+string(R_FOLDER)+string("TFIDF/"), "tfidf_PR", queryID, rank, *Y);

        // rank.clear() erases the memory
        // rank.erase() keeps the result in memory
        rank.erase(rank.begin(), rank.end());
    }
    cout<<" Ok"<<endl;
}
    //=================================================================================================

    //=================================================================================================
    // BM-25
    //=================================================================================================
if( BM25)
{
    // grid search for optimal b,k parameters

#if !TUNNEBM25
    float bestB;
    float bestK;

    if( FullColl && WITHSTEMING)
    {
        bestB = 0.378049;
        bestK = 2.04878;
    }
    else if( FullColl)
    {
        bestB = 0.346341;
        bestK = 2.2439;
    }
    else if( WITHSTEMING )
    {
        bestB = 0.347619;
        bestK = 3.57143;
    }
    else
    {
        bestB = 0.333333;
        bestK = 3.7619;
    }
#else
    // TUNING PARAMETERS
    float totK = 0;
    float totB = 0;
    float bestB;
    float bestK;
    size_t num =0;
    for( size_t i =1 ; i < 50; i++)
    {
        Query *q = qM->getQuery(i);
        vector<unsigned int> *Y = qM->getJudgements(i);

        if( q != NULL && Y != NULL)
        {
            pair<float,float> ret;
            ret = gridSearch(*q,i,*Y, 0.5 ,0.001);
            //pair<float,float> (bestB, bestK) = gridSearch(*q,i,*Y,1,0.1);
            //cout<<" bestB: "<<bestB<<" _ "<<bestK<<endl;
//            totB += bestB;
//            totK += bestK;
            totB += ret.first;
            totK += ret.second;
            ++num;
        }
    }
    cout <<"global best parameters: b = "<< totB/(float)num<< " K= "<< totK/(float)num<<endl;

    bestB = totB / (float)num;
    bestK = totK / (float)num;
#endif

    cout<<"testing BM25"; cout.flush();
    rank.erase(rank.begin(), rank.end());
    for( size_t queryID = 1; queryID<=50; ++queryID)
    {
        Query *q = qM->getQuery(queryID);
        vector<unsigned int> *Y = qM->getJudgements(queryID);
        if( q != NULL && Y != NULL)
        {
            bm25( *q, rank, bestK, bestB);

            //printResults(queryID, "tfidf", rank);
            saveResults(string(PROJECT_PATH)+string(R_FOLDER)+string("BM25/"), queryID, "bm25", rank);

            precisionRecall(string(PROJECT_PATH)+string(R_FOLDER)+string("BM25/"), "bm25_PR", queryID, rank, *Y);

            //cout<<" bm25 "<< queryID << endl;

            // rank.clear() erases the memory
            // rank.erase() keeps the result in memory
            rank.erase(rank.begin(), rank.end());
        }
    }
    cout<<" OK"<<endl;
}
    //=================================================================================================


    //=================================================================================================
    // ADA RANK
    //=================================================================================================
if (ADARANK)
{
#if SHUFFLE_Y

#if SHUFFLE_TEST
    string suffleType = "A";
    // define seed for the random, every run will create the same random
    srand(1540432678);
#else
    srand( 2343490 );
    string suffleType = "B";
#endif
#endif

    cout<<"testing AdaRank"; cout.flush();
    adaRank ada;

#if !BM25
#if FullColl && WITHSTEMING
    float bestB = 0.378049;
    float bestK = 2.04878;
#elif FullColl
    float bestB = 0.346341;
    float bestK = 2.2439;
#elif WITHSTEMING
    float bestB = 0.347619;
    float bestK = 3.57143;
#else
    float bestB = 0.333333;
    float bestK = 3.7619;
#endif
#endif

    struct qy{
        Query* q;
        vector<unsigned int> * Y;
    };
    vector< qy > vqy;

    qy q6;
    qy q7;
    // k fold ...

    for( size_t i =1 ; i < 50; i++)
    {
        Query *q = qM->getQuery(i);
        vector<unsigned int> *Y = qM->getJudgements(i);

        if( q != NULL && Y != NULL)
        {
            if( i == 6)
            {
                q6.q = q;
                q7.Y = Y;

            }
            else if(i == 7)
            {
                q7.q = q;
                q7.Y = Y;

            }
            else
            {
                qy tmp;
                tmp.q = q;
                tmp.Y = Y;
#if SHUFFLE_Y
            random_shuffle( tmp.Y->begin(), tmp.Y->end(), myrandom);
#endif
                vqy.push_back( tmp);
            }
        }
    }

    // k-fold
    // Test - Train
    // [6,7] - [ .... ]
    vector< Query *> vq;
    vector< vector<unsigned int >* > vy;

    for(auto it = vqy.begin(); it != vqy.end(); ++it)
    {
        vq.push_back( it->q);
        vy.push_back( it->Y);
    }
    cout<<"\nsize: "<<vq.size()<<endl;

    ada.setB_K( bestB, bestK);

    ada.train(vq, vy);

    cout<<"train done"<<endl;

    unsigned int queryID = 6;
    Query *q = qM->getQuery(queryID);
    vector<unsigned int> *Y = qM->getJudgements(queryID);

    assert( q != NULL);
    assert( Y != NULL);

    assert( q == q6.q);
    assert( Y == q6.Y);

    ada.test( *q, rank );

#if SHUFFLE_Y
    saveResults(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), queryID, string("adaRank_")+suffleType, rank);
    precisionRecall(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), string("adaRank_PR_")+suffleType, queryID, rank, *Y);
#else
    saveResults(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), queryID, "adaRank", rank);
    precisionRecall(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), "adaRank_PR", queryID, rank, *Y);
#endif

    rank.erase(rank.begin(), rank.end());

    queryID = 7;
    q = qM->getQuery(queryID);
    Y = qM->getJudgements(queryID);

    assert( q != NULL);
    assert( Y != NULL);
    assert( q == q7.q);
    assert( Y == q7.Y);

    ada.test( *q, rank );

#if SHUFFLE_Y
    saveResults(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), queryID, string("adaRank_")+suffleType, rank);
    precisionRecall(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), string("adaRank_PR_")+suffleType, queryID, rank, *Y);
#else
    saveResults(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), queryID, "adaRank", rank);
    precisionRecall(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), "adaRank_PR", queryID, rank, *Y);
#endif

    rank.erase(rank.begin(), rank.end());


    cout<<"query 6 and 7 done"<<endl;

    // [7] - [ .... ]

    adaRank ada3;
    ada3.setB_K( bestB, bestK);

    *(vq.end()-1) = q6.q;

#if SHUFFLE_Y
    assert(q6.Y != NULL);
    assert((qM->getJudgements(6))->size() > 0);
    vector<unsigned int> q6Y(*qM->getJudgements(6));
    random_shuffle( q6Y.begin(), q6Y.end(), myrandom);
    *(vy.end()-1) = &q6Y;
#else
    *(vy.end()-1) = q6.Y;
#endif

    cout<<"size: "<<vq.size()<<endl;
    ada3.train(vq, vy);
    cout<<"train done"<<endl;

    queryID = 7;
    q = qM->getQuery(queryID);
    Y = qM->getJudgements(queryID);

    assert( q != NULL);
    assert( Y != NULL);

    ada3.test( *q, rank );

#if SHUFFLE_Y
    saveResults(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), queryID, string("adaRank_only_")+suffleType, rank);
    precisionRecall(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), string("adaRank_PR_only_")+suffleType, queryID, rank, *Y);
#else
    saveResults(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), queryID, "adaRank_only", rank);
    precisionRecall(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), "adaRank_PR_only", queryID, rank, *Y);
#endif

    rank.erase(rank.begin(),rank.end());

    cout<<"query 7 done"<<endl;

    // [6] - [ .... ]

    adaRank ada2;
    ada2.setB_K( bestB, bestK);
    vq.push_back( q7.q);

#if SHUFFLE_Y
    vector<unsigned int> q7Y(*q7.Y);
    random_shuffle( q7Y.begin(), q7Y.end(), myrandom);
    vy.push_back( &q7Y );
#else
    vy.push_back( q7.Y);
#endif

    cout<<"size: "<<vq.size()<<endl;
    ada2.train(vq, vy);
    cout<<"train done"<<endl;

    queryID = 6;
    q = qM->getQuery(queryID);
    Y = qM->getJudgements(queryID);

    assert( q != NULL);
    assert( Y != NULL);

    ada2.test( *q, rank );

#if SHUFFLE_Y
    saveResults(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), queryID, string("adaRank_only_")+suffleType, rank);
    precisionRecall(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), string("adaRank_PR_only_")+suffleType, queryID, rank, *Y);
#else
    saveResults(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), queryID, "adaRank_only", rank);
    precisionRecall(string(PROJECT_PATH)+string(R_FOLDER)+string("AdaRank/"), "adaRank_PR_only", queryID, rank, *Y);
#endif

    cout<<"query 6 done"<<endl;
    rank.erase(rank.begin(), rank.end());


    cout<<" OK"<<endl;
}
    //=================================================================================================

        //End test
        //====

//        getline(std::cin, command);
//        if(command == "exit")
//            exit = true;
//    }

cout<< " Do you want to return to the menu ? ( y: yes, n: no, other: no)"<<endl;
getline(cin, answer);
if( answer == "y" || answer == "Y")
    goto askColl;



    cout<<"------------------------------------------------\n";
    cout<<"Deleting Allocated Memory"<<endl;
    delete fileM;
    delete invIndM;
    delete qM;
    cout <<"Done - End of Program!"<<endl;
    return 0;
}
