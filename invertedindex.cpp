#include "invertedIndex.h"
#include <algorithm>
#include <stdio.h>
#include <iostream>
#include <fstream>

#include "other/common.h"
#include "other/filesystem.h"
#include "File/bagofwords.h"

InvertedIndex* InvertedIndex::m_instance=NULL;

InvertedIndex::InvertedIndex()
{
}

InvertedIndex::~InvertedIndex()
{
    m_instance = NULL;

    cout<<" Deleting InvertexIndex:"<<endl;
    size_t progress= 0;
    float progressAdd = 1.f  / (float)m_InvertedIndex.size();

    for(m_it = m_InvertedIndex.begin(); m_it != m_InvertedIndex.end(); m_it++ )
    {
        progress ++;
        displayProgressBar(progress*progressAdd);
        (m_it->second).postingList->clear();
        delete m_it->second.postingList;
    }

    m_InvertedIndex.clear();
    cout<<endl;
}

void InvertedIndex::addBagOfWords(unsigned int id, BagOfWords *bag)
{

    for(vocabulary_t::iterator it_voc = (bag->m_vocabulary).begin(); it_voc != (bag->m_vocabulary).end(); it_voc++)
    {
        m_it = m_InvertedIndex.find( it_voc->first );

        if( m_it != m_InvertedIndex.end() )
        {   // duplicate token
            (m_it->second).freqCollection += it_voc->second;
            (m_it->second).postingList->push_back(id);
        }else{
            // new word
            content_t vs;
            vs.postingList = new vector<unsigned int>();
            vs.postingList->push_back(id);

            vs.freqCollection = it_voc->second;

            m_InvertedIndex.insert(PairStringVectorUInt(it_voc->first, vs));
        }
    }
}

unsigned int InvertedIndex::getFrequency(const string& token)
{
    m_it = m_InvertedIndex.find(token);

    if( m_it != m_InvertedIndex.end() )
    {
        return (unsigned int) (m_it->second).postingList->size();
    }
    return 0;
}



void InvertedIndex::writeFile(const string& fileName)
{

    size_t found = fileName.find_last_of('/');
    string path = fileName.substr(0,found+1);
    // make sure the path exist
    DIR *dir = opendir(path.c_str());
    if(dir == NULL)
    {
        //cout<<"directory not exist creating"<<endl;
        Create_Directory( path.c_str());
    }

    ofstream file(fileName.c_str());

    if( !file.is_open())
    {
        cout<<"Error: Opening the file "<< fileName << endl;
        return;
    }
    // this only create a line the same lenght of the name
    string bar(fileName.size(),'-');

    // header of the file
    file << bar << "\n";
    file << fileName << "\n";
    file << bar << "\n";
    file << "\n";


    //Progress bar
    cout<<endl;
    float progress= 0.0f;
    float progressAdd = 1.f  / (float)m_InvertedIndex.size();


    // Write all the inverted index format: word : [ id, id, ..., id]
    for(m_it = m_InvertedIndex.begin(); m_it != m_InvertedIndex.end(); m_it++ )
    {
        // progressbar
        progress += 1.0f;
        displayProgressBar( progress*progressAdd);
        //---

        file << m_it->first << " : [";

        vector<unsigned int> *ids = m_it->second.postingList;
        vector<unsigned int>::iterator it = ids->begin();

        // we know that min has to be one file
        file << " "<<*it;

        it++;
        for(; it != ids->end(); it++)
        {
            file << ", "<< *it;
        }
        file << "]"<<"\n";
    }

    cout<<endl;

    // Close the file
    file.close();
}

vector<unsigned int> * InvertedIndex::getPostingList(const string &token)
{
    m_it = m_InvertedIndex.find(token);

    if( m_it != m_InvertedIndex.end() )
    {
        return m_it->second.postingList;
    }
    return NULL;
}

bool compare(vector<unsigned int> *a, vector<unsigned int> *b){
    return a->size() <= b->size();
}

unsigned int InvertedIndex::getIntersecPostList(vector<string>& tokens, vector<unsigned int> &postList)
{
    vector< vector<unsigned int> * > list;
    size_t size = 0;
    for( auto t = tokens.begin(); t != tokens.end(); t ++)
    {
        vector<unsigned int> * a = getPostingList(*t);
        if( a != NULL){
            size += a->size();
            list.push_back(a);
        }else{
            cout<<"–GetIntersectPostList: Word not found: "<<*t<<endl;
        }
    }

    if( list.size() == 0)
    {
        postList.clear();
        return 0;
    }

    if( list.size() == 1)
    {
        for(auto it = (*(list.begin()))->begin(); it != (*(list.begin()))->end(); it++)
            postList.push_back(*it);
    }
    else
    {
        sort(list.begin(), list.end(),compare);

        vector<unsigned int> out_tmp(size);

        vector<unsigned int>::iterator l1_b = (list.at(0))->begin();
        vector<unsigned int>::iterator l1_e = (list.at(0))->end();
        vector<unsigned int>::iterator l2_b;// = (list.at(1))->begin();
        vector<unsigned int>::iterator l2_e;// = (list.at(1))->end();

        vector<unsigned int>::iterator l3_b = out_tmp.begin();
        vector<unsigned int>::iterator l3_e = l3_b;

        size_t i=1;
        do{
            l2_b = (list.at(i))->begin();
            l2_e = (list.at(i))->end();

            while (l1_b != l1_e && l2_b != l2_e)
             {
               if (*l1_b < *l2_b) ++l1_b;
               else if (*l2_b < *l1_b) ++l2_b;
               else {
                 *l3_e = *l1_b;
                 ++l3_e; ++l1_b; ++l2_b;
               }
             }

            //l3_e = set_intersection( l1_b, l1_e, l2_b, l2_e, l3_b);

            l1_b = l3_b;
            l1_e = l3_e;

            i++;
        }while( i < list.size() );

        l3_e = unique ( l1_b, l1_e);
        l1_e = l3_e;

        for( auto it= l1_b; it != l1_e; it++)
            postList.push_back(*it);
    }

    return (unsigned int) postList.size();
}


unsigned int InvertedIndex::getUnionPostList(vector<string>& tokens, vector<unsigned int> &postList)
{
    vector< vector<unsigned int> * > list;
    size_t size = 0;
    for( auto t = tokens.begin(); t != tokens.end(); t ++)
    {
        vector<unsigned int> * a = getPostingList(*t);
        if( a != NULL){
            size += a->size();
            list.push_back(a);
        }
//        else{
//            cout<<"–GetUnionPostList: Word not found: "<<*t<<endl;
//        }
    }

    if( list.size() == 0)
    {
        return 0;
    }
    else if( list.size() == 1)
    {
        for(auto it = (*(list.begin()))->begin(); it != (*(list.begin()))->end(); it++)
            postList.push_back(*it);
    }
    else
    {
        sort(list.begin(), list.end(),compare);

        vector<unsigned int> out_tmp( size );

        vector<unsigned int>::iterator l1_b = (list.at(0))->begin();
        vector<unsigned int>::iterator l1_e = (list.at(0))->end();
        vector<unsigned int>::iterator l2_b;// = (list.at(1))->begin();
        vector<unsigned int>::iterator l2_e;// = (list.at(1))->end();

        vector<unsigned int>::iterator l3_b = out_tmp.begin();
        vector<unsigned int>::iterator l3_e;

        size_t i=1;
        do{
            l2_b = (list.at(i))->begin();
            l2_e = (list.at(i))->end();

            l3_e = set_union( l1_b, l1_e, l2_b, l2_e, l3_b);

            l1_b = l3_b;
            l1_e = l3_e;

            i++;
        }while( i < list.size() );


        l3_e = unique ( l1_b, l1_e);
        l1_e = l3_e;
        for( auto it= l1_b; it != l1_e; it++)
            postList.push_back(*it);
    }

    return (unsigned int) postList.size();
}

