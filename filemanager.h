#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <string>
#include <map>
#include "File/file.h"
#include "invertedindex.h"

/*
 * Singleton Class: means only one instance of this class will
 *                  exist during the execution of the program
 */

using namespace std;

class FileManager
{
private:
    static FileManager* m_instance; // instance of the object
    PreProcessorEnum m_preprocessors;
    vector<File*> m_collection;
    bool m_isInit; // check that if we call init more than one only the first time do something
    unsigned int m_collectionFiles; // number of files read
    // m_collection.size()             number of files stored
    //                                 this diference is for knowing if there are empty files

    float m_averageLength;

    map<string, unsigned int> m_IDFileToPos;

    unsigned int m_collectionLength;


    /*
     * FileManager
     *
     * Private constructor: That's means only from in the class
     *                      can be created.
     */
    FileManager();

    /*
     * initListPreProcessors
     *
     * Inizialize the preprocessors
     */
    void initListPreprocessors();

public:
    /*
     * getInstance
     *
     * Returns the Instance of the object, if this instance is not
     * created, this is created.
     */
    inline static FileManager* getInstance(){
        if(m_instance == NULL)
            m_instance = new FileManager();
        return m_instance;
    }

    /*
     * ~FileManager
     *
     * Destroy the object and release the memory
     */
    ~FileManager();

    /*
     * init
     * @param path: path where the collection files are
     *              OR
     *              file that contains the list of all the files with the path
     *
     * Load all the files in the collection, and inizialize all
     * the structures with the files-bagOfWords and InvertedIndex
     * Return false if there is a problem loading
     *        true if all goes fine
     */
    bool init(string path = "Collection/" );

    /*
     * initFullCollection
     * @param path: path where the collection files .body are found
     *
     * Load all the files in the collection, and inizialize all
     * the structures with the files-BagOfWords and InvertedIndex
     * Returns false if there is a problem loading
     *         true if all goes fine
     */
    bool initFullCollection(string path);

    /*
     * initWithBagOfWords
     * @param path: path where the bagOfwords are found
     *
     * Load all the BagOfWods insted of the collection
     */
    bool initWithBagOfWords(const string& path);


    /*
     * addPreProcessor
     * @param preprocessor: flag from the enumeration (see File.h)
     *                      containing the name of the preprocessor
     *                      we want activate
     *
     * Add the preprocessor, this fuction has to be called before
     *  the init;
     */
    void addPreprocessor(PreProcessorEnum flag);

    /*
     * getSize
     *
     * Return the number of documents not empty loaded
     */
    inline unsigned int getSize(){
        return (unsigned int)m_collection.size();
    }

    /*
     * getNumberOfDocumentsReaded
     *
     * Return the number of documents readed
     */
    inline unsigned int getNumberOfDocumentsReaded(){
        return m_collectionFiles;
    }

    /*
     * initInvertedIndex
     *
     * Create the inverted index of all stored files in the vector
     */
    void initInvertedIndex();

    /*
     * writeFiles
     * @param path: (optional) path where the files will be stored
     *
     * Store the BagOfWords for every file, not empty, loaded
     */
    void writeFiles(const string& path = string("./"));

    /*
     * getTermFrequency
     *
     * Returns the frequency of a word in a document
     */
    unsigned int getTermFrequency(const string& token, unsigned int docID);

    /*
     * ShowEndsPreprocessors
     *
     * showResults of the preprocessors
     */
    void ShowEndsPreprocessors();

    /*
     * getDocIDFromName
     *
     * return the DocID from the name of the file
     */
    unsigned int getDocIDFromName(const string& name);

    /*
     * getCollectionLength
     *
     * Returns the total length of all documents in the collection
     */
    unsigned int getCollectionLength(){ return m_collectionLength;}

    /*
     * getAverageLength
     *
     * Returns the average length of the all documents in the collection
     */
    inline float getAverageLength(){return m_averageLength;}

    /*
     * getDocFromID
     *
     * Returns a pointer to the file with the id given
     */
    File* getDocFromID(unsigned int id);

    /*
     * getFrequencyInCollection
     *
     */
    inline unsigned int getFrequencyInCollection(const string &token)
    { return InvertedIndex::getInstance()->getFrequencyInCollection(token);}
};
#endif // FILEMANAGER_H
