#ifndef COMMON_H
#define COMMON_H

#include <assert.h>
#include <iostream>

#ifndef NDEBUG
#   define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message << std::endl; \
            std::exit(EXIT_FAILURE); \
        } \
    } while (false)
#else
#   define ASSERT(condition, message) do { } while (false)
#endif

#define SHOWPROGRESSBAR
#ifdef SHOWPROGRESSBAR
void displayProgressBar( float progress );
#else
inline void displayProgressBar( float progress) {}
#endif
#endif // COMMON_H
