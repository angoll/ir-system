#ifndef FILESYSTEM_H
#define FILESYSTEM_H

/*
 * Managing directories is not a Cross-Plataform so this define
 * is for having an implementation cross-plataform for managing directories
 *
 * This file is for managing directories
 * Create, know if there are directories etc..
 *
 */
//_WIN32
#ifdef __MINGW32__
    // For windows OS with compiler Mingw32
#include "dirent_win.h"
#include <windows.h>

// TODO: Check change: if error producced change LPCSTR to LPCSWSTR
#define Create_Directory(x) CreateDirectory( (LPCWSTR) x, NULL) //Before LPCWSTR

#elif defined(__unix__) || defined(__unix) || (defined(__APPLE__) && defined(__MACH__))
    // For the others OS

#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>

#define Create_Directory(x) mkdir(x,0777)

#else

#pragma message("Program not tested with this OS or Compiler – Only tested in MacOs and windows with MingWC")

#endif

#endif // FILESYSTEM_H
