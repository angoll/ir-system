#include "common.h"

#include <iostream>
#include <string>
#include <sstream>

#ifdef SHOWPROGRESSBAR
std::string progressBar("");
void displayProgressBar( float progress )
{
    // erase the previous bar
    std::cout<< std::string( progressBar.length(),'\b');

    // creates a progress bar:
    int barWidth = 40;

    progressBar = "[";
    int pos = barWidth * progress;
    for( int i=0; i< barWidth; ++i) {
        if( i< pos) progressBar+="=";
        else if( i== pos) progressBar += ">";
        else progressBar += " ";
    }
    progressBar += "] ";
    std::ostringstream ss;
    ss << (int ) (progress * 100.f);
    progressBar += ss.str();
    progressBar += " %";

    std::cout<< progressBar;//<<"\r";
    std::cout.flush();
}
#endif
