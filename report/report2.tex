\documentclass{article}

\usepackage[margin=1in]{geometry}
\usepackage{amsmath,amssymb}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{parskip}
\usepackage{multicol}
\usepackage{graphicx}



\title{Building an IR System}
       

\author{\\ Andreu Gonzalez Llinas, 10656677 \\ 
		   Camelia Simoiu, 10664122 \\
		   Hongpei Xu, 10629025 }

\begin{document}
\maketitle
\newpage

\section{Introduction and System Requirements}

We have implemented an information retrieval system that runs three models: tfidf, BM25, and AdaRank, on the sample CSIRO collection (262 documents), as well as the full collection (37,0715 documents, 33,4248 non-empty documents). The system is programmed from scratch except the porter's algorithm, we have used C++ as a programming language using a compiler that supports C++11. Using the full collection introduces a number of computing challenges. We have run the program successfully on MacOs and windows 7 with minGW32. We have overclocked the limit of 2GB (in a 32bit system) using up to 3GB of RAM memory per program. We believe that it should also work in Linux. 


\section{Pre-processing}

For each file, we store a 'bag of words', which we later assemble to create the inverted index. We have incorporated the following pre-processing steps:

\begin{itemize}

\item Tokenization: we have defined tokens to be segments of the document that are delimited by spaces
\item Case Folding: removes capitalization and reduces all characters of the token to lower case
\item Punctuation and accent removal (accents are transformed to the letter without the accent, all punctuation is removed from tokens)
\item HTML codes (eg. \&amp; \&\#160; \&\#x60; \&acute;) are recognized, processed, an transformed to their respective values
\item Stop word removal (Appendix D contains a complete list of all stop words removed)
\end{itemize}

The total count of unique tokens in the inverted index was 23,354 in the sample collection, and 1,092,005 in the full collection after all the above pre-processing steps were performed. 
 

%
%\section{Overview of IR Models}
%
%\subsection{tf-idf}
%
%As a baseline model, we have implemented the Term Frequency Inverse Document Frequency model, a statistic that reflects how important a word is to a document in a collection. The tf-idf value increases proportionally to the number of times a word appears in the document, but is offset by the frequency of the word in the collection, which helps to control for the fact that some words are much more common than others. 

%The tf-idf equation is included below.

%\begin{align}
%	Score(q,d) = \sum\limits_{t \in q \cap d}						(1+logtf_{t,d})log(\frac{N}{df_t})
%\end{align}

 
\section{Okapi BM25 Parameter Optimization}

The Okapi BM25 model is one of the best probabilistic weighting schemes, which uses a bag-of-words approach (ie. a retrieval function) to rank a set of documents based on the query terms appearing in each document, regardless of the inter-relationship between the query terms within a document (e.g., their relative proximity). We have optimized the two free parameters, b and K, where 0 $\leq$ b $\leq$ 1 represents the document length scaling. As the calibration of b and K is dependent on the collection, the parameters were tuned for four cases: the full collection (with and without stemming), and the sample collection (with and without stemming) in order to investigate how this might influence the optimal parameter set. We conducted a grid search, with steps of 1 for K, and 0.1 for b.\\

%The BM25 equation is included below.

%\begin{align}
%	Score(q,d) = \sum\limits_{t \in q} 
%	[log(\frac{N}{df_t})]\cdot 
%	\frac{(k_1 +1)tf_{t,d}}{k_1[(1-b)
%	+b(\frac{L_d}{L_{ave}})]
%	+tf_{t,d}}
%\end{align}

Table 1. BM25 parameter optimization
\begin{center}
\begin{tabular}{ *8l }    \toprule 
	\phantom{absssssc} &
	\multicolumn{2}{c}{Sample Collection} & 
	\phantom{abssc} & 
	\multicolumn{2}{c}{Full Collection} \\\toprule
	stemming & b & K && b & K    \\\midrule
	Without Porter & 0.3463 & 2.2439 && 0.3333 & 3.7619 \\ 
	With Porter    & 0.3780 & 2.0488 && 0.3476 & 3.5714 \\ 
 \hline
 \end{tabular}
\end{center}


Stemming seems to increase the value of b for both the full collection (9.16\% from 0.3463 to 0.3780) and the sample collection (4.29\% from 0.3333 to 0.3457. Stemming effectively contracts words down to their 'base root'. This would imply that the number of unique tokens would decrease, and the term frequency of any one token would, on average, increase. The inverse document frequency would also increase, since as N, the total number of documents in the collection, remains constant, the number of documents where the (now more common term) appears would increase. In contrast, stemming is seen to decrease the optimal value for K (-8.70\% from 2.2439 to 2.0488) for the full collection, and (-5.06\% from 3.7619 to 3.5714) for the sample collection. For both K and b, the effect is more pronounced in the full collection, which may be attributed to a greater variation in document lengths for the full collection. If there are more documents that are either extremely long or extremely short, a higher document length scaling factor will be required to account for this difference, while K decreases, and offsets this effect.




\section{AdaRank Overview}

AdaRank is a boosting algorithm that utilizes a linear combination of 'weak rankers' as its model. The objective of learning is to create a ranking function, such that for each query, the relevant documents can be assigned scores, and rankings according to those scores. The learning process tries to minimize the loss function which represents the disagreement between the ranks produced by \textit{f}, and the list of ranks \textit{$y_{i}$}, for all the queries. We use Mean Average Precision (MAP) as the performance measure and the seven features listed in Li and Xu (2007). At each round, the best weak ranker out of the seven, is chosen, and used to expand \textit{f} with its associated weight. Initially, AdaRank sets equal weights to every query, however in subsequent rounds, the weights of those queries that were not well ranked by \textit{f}, the model so far, are increased. As a result, the learning at the next round will be focused on the creation of a weak ranker that can work on ranking of those 'hard' queries.

Out of the 50 queries, only 42 had corresponding QLRL files, so only these were used in the training/testing set. Having human annotated rankings of the documents would be the ideal standard to compare against. However, in the absence of these, all relevant documents for each query was assigned an arbitrary ranking. In order to test to what degree this assumption could influence our results, we ran the model using two more random permutations of \textit{y} where the rankings are shuffled, using a different randomly assigned ranking each time. Only the top 25 documents retrieved by \textit{f} were tested against \textit{y}. Leave-one-out cross-validation was performed with queries 6 and 7 respectively, meaning that training was performed on 41 queries, and testing on one query (either 6 or 7).

\section{Model Performance}

We have compared the performance of the three models with and without Porter's stemming, using Mean Average Precision, Precision at K and Recall at K (K = 10,20,R where R is the number of relevant documents). Precision is the probability that a (randomly selected) retrieved document is relevant.
Recall is the probability that a (randomly selected) relevant document is retrieved in a search. We have tested our system on queries 6 and 7 using the sample collection, and all 42 queries using the full collection. The metric scores observed for the sample collection are artificially high, as a higher proportion of relevant documents for queries 6 and 7 were chosen for the sample collection. The scores decrease substantially when tested on the full collection (Tables 1 and 2 below).

\subsection{Effect of Porter Stemming}

Stemming essentially merges words together, in a process of generalization, which should lead to an improvement in recall, and at the same time, a loss in precision. On the one hand, words which ought to be merged together (such as "water" and "watery") may remain distinct after stemming, and words which have distinct meanings may be incorrectly conflated (e.g., "classified" and "classification"). These are known as understemming errors and overstemming errors respectively. Overstemming lowers precision and understemming lowers recall. Since no stemming at all implies zero overstemming but maximum understemming errors, this would lead to a low recall and a high precision. 

For the sample collection, stemming improves precision for tf-idf and BM25 according to all metrics. This was a surprising result at first, however may be explained by the fact that 1) the sample collection is artificially engineered to have more relevant documents than normal to be recalled, and 2) the language of the 2 queries are such that stemming would improve their precision. The first query, "sustainable ecosystems" would most likely benefit from stemming, as there are many related words that could also return relevant documents: "sustainability, sustainably, sustainability, sustainment, ecosystems", etc. The second query again may benefit from some stemming, but probably not as much. "Air guitar textile sensors" does not have as many related words that, conflating, would lead to relevant results. 

For the full collection, we observe more realistic results, such that the precision and relevance scores decrease substantially (from a MAP sore of 70\% and an average recall of 53\% for the sample collection, to a MAP score of 19\% and an average recall of -7\% for the full collection). Stemming is still shown to increase precision, but also lower recall. Effectively, the documents that are retrieved are very accurate or relevant to the query, but not as many as recalled. For BM25, we observe an increase in precision and recall for the sample collection, and a decrease in precision and recall for the full collection. Due to the need for training, AdaRank was only run on the full collection. In this case again, we see that stemming hurt performance, leading to a marginal overall increase of 0.5 in MAP, and a decrease in recall. This is to be expected, given the results from BM25. The strongest weak ranker used in our feature set for AdaRank was BM25, whose performance in the previous experiment was seen to decrease from stemming.\\ 



\subsection{Analysis of Results}

We achieved the best results using BM25, for both the sample collection and the full collection. Although BM25 is a powerful ranker, we expected to outperform this and 'boost' performance using AdaRank. After much testing and review of the code, we realise that Adarank selects mostly feature 1 and 5 at most iterations for both queries, and not BM25 as we expected. We achieved very good results for query 7, however we perform poorly on query 6. This may be due to a number of factors. Li and Xu tested AdaRank on a number of different collection with characteristics that perhaps are much different to ours and also used more queries and/or features. In testing on OHSUMED Data, the 7 features which we have used were applied, 106 queries were used, and documents had human-annotated relevance scores at three levels of relevance.  In the WSJ and AP experiment, the same 7 features were used, 200 queries were used for training, and documents had binary judgements. When using the GOV Dataset, they had 50 queries, binary judgement, but extracted 14 features from the dataset. This was the dataset they performed most poorly on, and cite the main cause as being not having enough queries to train on.

This could explain our inability to exceed BM25's performance, however not the fact that we score lower than BM25. We have analysed query 6 and query 7 independently, and performed numerous tests in order to try to uncover the reason for these unusual results. We notice that AdaRank performs surprisingly well on query 7, but extremely poorly on query 6. We have tried to disable the two features that AdaRank consistently chooses (1 and 5), and found that other features except BM25 are chosen, leading to poorer precision scores. This implies that the features are indeed working correctly. We have also tried to boost BM25 by removing the "log" and multiplying by 10, however only when all other features are disabled, did the AdaRank algorithm choose BM25 as the best weak ranker and obtain the same result. Shuffling the \textit{y} vector also did not seem to yield any difference in results. This is to be expected however, since indifferent of the random ranking assignment to documents, we always search through the entire vector \textit{y}, whose contents of the relevant documents always remains the same. Detailed results from these experiments have been included in Appendix C. We would have hoped to resolve these issues, however due to the time constraints did not have time to perform any other tests.



\newpage

\section{Appendix A}



Table 1. Models evaluated using the sample collection (QREL \#6 \& \#7)

\begin{center}
\begin{tabular}{ *8l }    \toprule
\emph{Model}  & MAP & P@10 & P@20 & R@10 & R@20\\\midrule
tfidf\_withoutPorter & 0.6839 & 0.75 & 0.8 & 0.3008 & 0.6410 \\ 
tfidf\_porter 	    & 0.7014 & 0.85 & 0.825 & 0.3813 & 0.6802 \\
Stemming effect (\%) & 2.56 & 13.33 & 3.12 & 26.76 & 6.11 \\
\phantom{abc} \\
BM25\_withoutPorter & 0.7068 & 0.8 & 0.825 & 0.3202 & 0.6609 \\ 
BM25\_porter	    & 0.7417 & 0.95 & 0.875 & 0.3603 & 0.7012 \\
Stemming effect (\%) & 4.94 & 18.75 & 6.06 & 12.52 & 6.09 \\\bottomrule
 \hline
\end{tabular}
\end{center}

\vspace{0.5cm}

Table 2. Models evaluated using the full collection (QREL \#6 \& \#7)

\begin{center}
\begin{tabular}{ *8l }    \toprule
\emph{Models}  & MAP & P@10 & P@20 & R@10 & R@20\\\midrule
tfidf\_withoutPorter & 0.1267 & 0.2400 & 0.2180 & 0.2193 & 0.3762 \\ 
tfidf\_porter        & 0.1938 & 0.3980 & 0.3610 & 0.2012 & 0.3524 \\
Stemming effect (\%) & 52.96 & 65.83 & 65.60 & -8.25 & -6.33 \\
\phantom{abc} \\
BM25\_withoutPorter & 0.3043 & 0.6520 & 0.5940 & 0.3134 & 0.5629 \\ 
BM25\_porterStemming & 0.3044 & 0.6320 & 0.5770 & 0.3048 & 0.5524 \\
Stemming effect (\%) & 0.03 & -3.07 & -2.86 & -1.92 & -1.86\\
\phantom{abc} \\
AdaRank\_withoutPorter & 0.1990 & 0.4500 & 0.5750 & 0.1832 & 0.3030\\ 
AdaRank\_porterStemming & 0.2000 & 0.4500 & 0.3500 & 0.1801 & 0.2803 \\
Stemming effect (\%) & 0.50 & 0.00 & -6.67 & -2.10 & -7.49 \\\bottomrule
 \hline
\end{tabular}
\end{center}



%Table 3. Models evaluated using the sample collection (QREL \#6).\\
%
%\begin{tabular}{ *8l }    \toprule
%\emph{Models}  & MAP & P@10 & P@20 & P@R \\\midrule
%tfidf\_withoutPorter & 0.6192 & 0.5000 & 0.6000 & 0.6486 \\ 
%tfidf\_porter	     & 0.632 & 0.7000 & 0.7000 & 0.6622 \\
%BM25\_withoutPorter  & 0.6631 & 0.6000 & 0.6500 & 0.6757 \\
%BM25\_porterStemming & 0.7028 & 0.8000 & 0.7500 & 0.6757 \\\bottomrule
% \hline
%\end{tabular}\\\\
%
%
%
%Table 4. Models evaluated using the sample collection (QREL \#7)\\
%
%\begin{tabular}{ *8l }    \toprule
%\emph{Model}  & MAP & P@10 & P@20 & P@R \\\midrule
%tfidf\_withoutPorter & 0.7498 & 1.0000 & 1.0000 & 0.5439 \\
%tfidf\_porter	     & 0.7723 & 1.0000 & 1.0000 & 0.5789 \\
%BM25\_withoutPorter & 0.7713 & 1.0000 & 1.0000 & 0.5965 \\ 
%BM25\_porter	    & 0.7985 & 1.0000 & 1.0000 & 0.6667 \\\bottomrule
% \hline
%\end{tabular}\\\\


\newpage

\section{Appendix B}

The following graphs show Average Precision for the three models (tf-idf, BM25, and AdaRank).\\

Figure 1. Precision for tf-idf and BM25 (full collection)
\begin{figure}[h]
\centering
\includegraphics[height=5cm,width=0.75\textwidth]{both_full.png}
%\caption{\label{fig:tfidfPorter}Precision for BM25.}
\end{figure}


Figure 2. Precision for tf-idf (sample collection)

% h = here
\begin{figure}[h]
\centering
\includegraphics[height=5cm,width=0.75\textwidth]{tfidf.png}
%\caption{\label{fig:tfidfPorter}Precision for tf-idf.}
\end{figure}

Figure 3. Precision for BM25 (sample collection)
\begin{figure}[h]
\centering
\includegraphics[height=5cm,width=0.75\textwidth]{bm25.png}
%\caption{\label{fig:tfidfPorter}Precision for BM25.}
\end{figure}


%Figure 3. Precision for AdaRank
%\begin{figure}[h]
%\centering
%\includegraphics[height=5cm,width=0.75\textwidth]{AdaRank.png}
%%\caption{\label{fig:tfidfPorter}Precision for AdaRank.}
%\end{figure}

\newpage

\section{Appendix C}

The following table contains precision scores for queries 6 and 7 on the AdaRank tests performed.


Figure 1. Precision scores for AdaRank experiments (full collection, QREL \#6 \& QREL \#7)

% h = here
\begin{figure}[h]

\includegraphics[height=15cm,width=1.75\textwidth]{AdaRankQRELs.png}
%\caption{\label{fig:tfidfPorter}Precision for tf-idf.}
\end{figure}


\newpage

\section{Appendix D}

Below is the complete List of stop words removed. This list is the stop word list used by MySQL FullText feature.

\begin{multicols}{3}
a\\
able\\
about\\
across\\
after\\
all\\
almost\\
also\\
am\\
among\\
an\\
and\\
any\\
are\\
as\\
at\\
be\\
been\\
but\\
by\\
can\\
cannot\\
could\\
dear\\
did\\
do\\
does\\
either\\
else\\
ever\\
every\\
for\\
from\\
get\\
got\\
had\\
has\\
have\\
he\\
her\\
hers\\
him\\
his\\
however\\
i\\
\columnbreak
if\\
in\\
into\\
is\\
it\\
its\\
just\\
let\\
like\\
likely\\
may\\
me\\
might\\
most\\
must\\
my\\
neither\\
no\\
nor\\
not\\
of\\
off\\
often\\
on\\
only\\
other\\
our\\
own\\
rather\\
said\\
say\\
says\\
she\\
should\\
since\\
so\\
some\\
than\\
that\\
the\\
their\\
them\\
then\\
there\\
these\\
they\\
\columnbreak
this\\
tis\\
to\\
too\\
twas\\
us\\
wants\\
was\\
we\\
were\\
what\\
when\\
where\\
which\\
while\\
who\\
whom\\
why\\
will\\
with\\
would\\
yet\\
you\\
your\\

\end{multicols}



\end{document}