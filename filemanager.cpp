#include "filemanager.h"
#include "File/bagofwords.h"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <sstream>

#include "other/filesystem.h"
#include "invertedindex.h"

#include "other/common.h"
#include <climits>

FileManager *FileManager::m_instance=NULL;

FileManager::FileManager():
    m_preprocessors(None),
    m_isInit(false),
    m_collectionFiles(0),
    m_collectionLength(0)
{
}

FileManager::~FileManager()
{
    cout<<" Deleting Collection:"<<endl;
    size_t progress= 0;
    float progressAdd = 1.f  / (float)m_collection.size();
    for(vector<File*>::iterator it = m_collection.begin(); it != m_collection.end(); it++)
    {
        progress ++;
        displayProgressBar(progress*progressAdd);
        delete *it;
    }
    m_collection.clear();
    m_IDFileToPos.clear();
    m_instance = NULL;

    if( list_preprocessor != NULL)
    {
        for(vector<PreProcessor *>::iterator it=list_preprocessor->begin(); it != list_preprocessor->end(); it++)
        {
            (*it)->ShowEnd();
            delete *it;
        }
        delete list_preprocessor;
        list_preprocessor = NULL;
    }
    cout<<endl;

}


bool FileManager::init(string path)
{
    if(m_isInit)
        return true;
    // ==============================================
    initListPreprocessors();
    // ==============================================

    // Know if is a path or a file
    if(path.find_last_of(".") == path.npos){
        // it's a path
        if(path.substr(path.length()-1) != "/"){
            path += "/";
        }
        DIR *dir = opendir(path.c_str());
        if(dir != NULL)
        {   // read the entries of the folder
            struct dirent *direntry = NULL;
            while( (direntry = readdir(dir)) )
            {
                // ignore files like "."*
                if(direntry->d_name[0] != '.')
                {   // check that is a .txt file
                    string name = direntry->d_name;
                    if(name.substr(name.length()-4) == ".txt")
                    {
                        File *collectionFile = new File(path + name);
                        collectionFile->process();

                        if( !collectionFile->isFileEmpty() )
                        {
                            m_IDFileToPos.insert( pair<string,unsigned int>( collectionFile->getName(),m_collection.size()));
                            m_collection.push_back(collectionFile);
                        }
                        m_collectionFiles++;
                    }
                }
            }
            closedir(dir);
        }else{
            // problem with the path
            cout<<"directory not exist"<<endl;
            return false;
        }
//===================================================================
    }else{
        // it's a file
        ifstream file(path.c_str());
        if(!file.is_open())
        {
            cout << "Error: Opening the file "<< path<<endl;
            return false;
        }

        // Read the content of the file line by line
        string line;
        while(getline(file,line))
        {
            // process the line to get only the filename
            //cout << line <<endl;
            File *collectionFile = new File(line);
            collectionFile->process();

            if( !collectionFile->isFileEmpty() )
            {
                m_IDFileToPos.insert( pair<string,unsigned int>( collectionFile->getName(),m_collection.size()));
                m_collection.push_back(collectionFile);
            }
            m_collectionFiles++;
        }
        file.close();
    }

    m_collectionLength = 0;
    for(vector<File*>::iterator it = m_collection.begin(); it != m_collection.end(); it++)
    {
        m_collectionLength += (*it)->getDocLength();
    }

    m_averageLength = (float)getCollectionLength() / (float)m_collection.size();

    m_isInit = true;
    return true;
}

bool FileManager::initFullCollection(string path)
{
    if(m_isInit)
        return true;
    // ==============================================
    initListPreprocessors();
    // ==============================================


    vector<string> bodyFileName;

    // Read all the path of the collection
    if(path.substr(path.length()-1) != "/"){
        path += "/";
    }
    DIR *dir = opendir(path.c_str());
    if(dir != NULL)
    {   // read the entries of the folder
        struct dirent *direntry = NULL;
        while( (direntry = readdir(dir)) )
        {
            // ignore files like "."*
            if(direntry->d_name[0] != '.')
            {   // check that is a .body file
                string name = direntry->d_name;
                // only reads the .body files
                if(name.substr(name.length()-5) == ".body")
                {
                    // store all the filenames
                    bodyFileName.push_back(path+name);
                }
            }
        }
        closedir(dir);
    }else{
        // problem with the path
        cout<<"directory not exist"<<endl;
        return false;
    }

    // open the file and parse it to create all the subfiles
    ifstream bodyFile;

    float progress= 0.0f;
    float progressAdd = 1.f  / (float)bodyFileName.size();


    cout<<endl;

    char* buffer = new char[ 12915695 ]; // max file size

    for( auto name = bodyFileName.begin(); name != bodyFileName.end(); name++)
    {
        progress += 1.0f;
        displayProgressBar( progress*progressAdd);


        bodyFile.open(*name, ifstream::binary );

        if(!bodyFile.is_open()){
            cout<<"Error: opening the file "<< *name<<endl;
            continue;
        }

        // now parse all the files inside the file format
        // <DOC>
        // <DOCNO>.....</DOCNO>
        // <TEXT>......</TEXT>
        // </DOC>

        // get length of file:
        bodyFile.seekg (0, bodyFile.end);
        size_t bodyFileSize = bodyFile.tellg();
        bodyFile.seekg (0, bodyFile.beg);

        if( bodyFileSize > 12915695 )
        {
            delete[] buffer;
            buffer = new char[bodyFileSize];
        }

        //char* buffer = new char[bodyFileSize];

        // load all the file in memory
        bodyFile.read(buffer, bodyFileSize);

        if(!bodyFile)
            cout<< "error only "<< bodyFile.gcount() <<" could be read"<<endl;

        bodyFile.close();

        // parse the buffer
        bool tagDOC = false;
        bool tagDOCNO = false;
        bool tagTEXT = false;
        string tmp(""); tmp.reserve(10);
        string str_DOCNO(""); str_DOCNO.reserve(16);
        string str_TEXT(""); str_TEXT.reserve(512);

        size_t i = 0;
        while( i < bodyFileSize )
        {
            if( buffer[i] == '<' )
            {
                ++i;
                while( i < bodyFileSize && buffer[i] != '>')
                {
                    if( buffer[i] == '<')
                    {
                        // incomplete tag or simple a <
                        if( tagDOC )
                        {
                            if( tagDOCNO )
                                str_DOCNO += "<" + tmp;
                            else if( tagTEXT )
                                str_TEXT += "<" + tmp;
                        }
                        tmp.clear();
                    }
                    else
                    {
                        tmp += buffer[i];
                    }
                    ++i;
                }
                if( tmp == "DOC")
                {
                    tagDOC = true;
                }
                else if ( tmp == "/DOC")
                {
                    ASSERT( !tagDOCNO && !tagTEXT, str_DOCNO);
                    // end of collecting data for the first file
                    //cout<< str_DOCNO <<endl;
                    // create file
                    File *collectionFile = new File(str_DOCNO, str_TEXT);

                    if( !collectionFile->isFileEmpty() )
                    {
                        m_IDFileToPos.insert( pair<string,unsigned int>(str_DOCNO,m_collection.size()));
                        m_collection.push_back(collectionFile);
                    }

                    m_collectionFiles++;

                    // reset vars
                    tagDOC = false;
                    str_DOCNO.erase(0,str_DOCNO.length());
                    str_TEXT.erase(0, str_TEXT.length());
                }
                else if ( tmp == "DOCNO")
                {
                    tagDOCNO = true;
                }
                else if ( tmp == "/DOCNO")
                {
                    tagDOCNO = false;
                    //cout<<str_DOCNO<<endl;
                }
                else if ( tmp == "TEXT")
                {
                    tagTEXT = true;
                }
                else if ( tmp == "/TEXT")
                {
                    tagTEXT = false;
                }
                else{
                    // other tag
                    if( tagDOC )
                    {
                        if( tagDOCNO )
                            str_DOCNO += "<" + tmp + ">";
                        else if( tagTEXT )
                            str_TEXT += "<" + tmp + ">";
                    }
                }
                tmp.clear();

            }else if(tagDOC)
            {
                if( tagDOCNO )
                    str_DOCNO += buffer[i];
                else if( tagTEXT )
                    str_TEXT += buffer[i];

            }
            // else ignore
            ++i;
        }
    }

    delete[] buffer;
    cout<<endl;

    m_collectionLength = 0;
    for(vector<File*>::iterator it = m_collection.begin(); it != m_collection.end(); it++)
    {
        m_collectionLength += (*it)->getDocLength();
    }

    m_averageLength = (float)getCollectionLength() / (float)m_collection.size();

    m_isInit = true;
    return true;
}


bool FileManager::initWithBagOfWords(const string& path)
{
    if(m_isInit)
        return true;
    // ==============================================
    initListPreprocessors();
    // ==============================================

    ifstream fileBag;

    fileBag.open( path);

    if( !fileBag.is_open() )
    {
        cout<<"Error: opening the file "<< path <<endl;
        return false;
    }

    string line;
    size_t n_files = 0;
    string nameDoc("");
    BagOfWords *BOW = NULL;


    // read the header 3 first lines
    // <NUMFILES> # </NUMFILES>
    getline( fileBag, line);
    while( line =="")
        getline(fileBag,line);

    assert(line == "<NUMFILES>");
    getline( fileBag, line);
    std::istringstream iss( line );
    iss >> n_files;
    getline( fileBag, line);
    assert(line == "</NUMFILES>");


    size_t progress= 0;
    float progressAdd = 1.f  / (float)n_files;
    cout<<endl;

    for( size_t i=0; i< n_files; i++)
    {
        progress ++;
        displayProgressBar(progress*progressAdd);

        bool tagDOC = false;
        bool tagBAGOFWORDS = false;

        while( getline(fileBag,line))
        {
            if( line == "</BAGOFWORDS>" )
            {
                getline(fileBag,line);
                if( line == "</DOC>")
                {   // end of Doc
                    tagBAGOFWORDS = false;
                    tagDOC = false;
                    break;
                }
            }
            else if( tagDOC && tagBAGOFWORDS )
            {
                unsigned int freq;
                size_t found = line.find_last_of(':');
                string word(line.substr(0,found-1));
                istringstream iss2(line.substr(found+1));
                iss2>>freq;
                BOW->addWord(word, freq);
            }
            else if( line == "<DOC>")
            {
                tagDOC = true;
            }
            else if( line == "<DOCNAME>")
            {
                getline(fileBag,line);
                nameDoc = line;
                getline(fileBag,line);
                assert(line == "</DOCNAME>");

                if(tagDOC)
                {   // have new doc and name
                    // create file
                    File *collectionFile = new File( nameDoc , NULL );
                    BOW = collectionFile->getBagOfWords();

                    m_IDFileToPos.insert( pair<string,unsigned int>(nameDoc,m_collection.size()));
                    m_collection.push_back(collectionFile);

                    m_collectionFiles++;
                }

            }
            else if( line == "<BAGOFWORDS>")
                tagBAGOFWORDS = true;
//            else
//                cout<<"*_*"<<line<<endl;
        }
    }
    fileBag.close();
    cout<<endl;

    m_collectionLength = 0;
    for(vector<File*>::iterator it = m_collection.begin(); it != m_collection.end(); it++)
    {
        m_collectionLength += (*it)->getDocLength();
    }

    m_averageLength = (float)getCollectionLength() / (float)m_collection.size();

    m_isInit = true;
    return true;
}


void FileManager::addPreprocessor(PreProcessorEnum flag)
{
//    m_preprocessors = flag;
    if( flag != None )
        m_preprocessors = m_preprocessors | flag;
    else
        m_preprocessors = None;

    cout<< showPreProcessors(m_preprocessors)<<endl;
}

void FileManager::initListPreprocessors()
{

    // init preprocessors
    assert( list_preprocessor == NULL );
    list_preprocessor = new vector<PreProcessor *>();

    // TODO: Preprocessor flags
    // ALERT: the order of the flags check is the order that the processors will follow
    if( m_preprocessors & STEMING )
        list_preprocessor->push_back( (PreProcessor *)new porterAlgorithm() );
//        if(flags & DOMAIN) {
//            m_preprocessor->push_back((PreProcessor *)new domain());
//        }
//        if(flags & EMAIL) {
//            m_preprocessor->push_back((PreProcessor *)new email());
//        }
    if(m_preprocessors & COUNTER)
        list_preprocessor->push_back((PreProcessor * )new Counter());
}

void FileManager::ShowEndsPreprocessors()
{
    if( list_preprocessor != NULL)
    {
        for(vector<PreProcessor *>::iterator it=list_preprocessor->begin(); it != list_preprocessor->end(); it++)
        {
            (*it)->ShowEnd();
            delete *it;
        }
        delete list_preprocessor;
        list_preprocessor = NULL;
    }
    list_preprocessor = new vector<PreProcessor *>();

    // TODO: Preprocessor flags for Querys
    // ALERT: the order of the flags check is the order that the processors will follow
    if( m_preprocessors & STEMING )
        list_preprocessor->push_back( (PreProcessor *)new porterAlgorithm() );
//        if(flags & DOMAIN) {
//            m_preprocessor->push_back((PreProcessor *)new domain());
//        }
//        if(flags & EMAIL) {
//            m_preprocessor->push_back((PreProcessor *)new email());
//        }

}

void FileManager::initInvertedIndex()
{
    InvertedIndex *index = InvertedIndex::getInstance();

    cout<<endl;
    float progress= 0.0f;
    float progressAdd = 1.f  / (float)m_collection.size();

    unsigned int id = 0;
    for(vector<File*>::iterator it = m_collection.begin(); it != m_collection.end(); it++)
    {
        progress += 1.0f;
        displayProgressBar( progress*progressAdd);

        index->addBagOfWords(id,(*it)->getBagOfWords());
        id++;
    }
    cout<<endl;

}

void FileManager::writeFiles(const string& path)
{
    if(m_isInit)
    {
        // make sure the path exist
        DIR *dir = opendir(path.c_str());
        if(dir == NULL)
        {
            //cout<<"directory not exist creating"<<endl;
            Create_Directory( path.c_str());
        }

        cout<<endl;
        float progress= 0.0f;
        float progressAdd = 1.f  / (float)m_collection.size();

        ofstream saveFile(string(path+"BagsOfWords.txt").c_str());
        if(!saveFile.is_open())
        {
            cout<<"Error: Opening the file "<< path+"BagsOfWords.txt" << endl;
            return;
        }


        // writing the header of the file:
        saveFile<<"<NUMFILES>\n"<<m_collection.size()<<"\n</NUMFILES>\n"<<endl;


        for(vector<File*>::iterator it = m_collection.begin(); it != m_collection.end(); it++)
        {
            progress += 1.0f;
            displayProgressBar( progress*progressAdd);

            (*it)->writeFile(saveFile);
        }

        saveFile.close();
        cout<<endl;
    }
}

unsigned int FileManager::getTermFrequency(const string& token, unsigned int docID){
    // the position where the file located in the vector of files
    return m_collection.at(docID)->getFrequency(token);
}

//unsigned int FileManager::getCollectionLength(){

//    unsigned int tot = 0;

//    for(vector<File*>::iterator it = m_collection.begin(); it != m_collection.end(); it++)
//    {
//        tot += (*it)->getDocLength();
//    }
//    return tot;
//}


File* FileManager::getDocFromID(unsigned int id){
    if(id < m_collection.size()) {
        return m_collection.at(id);
    }
    return NULL;
}


unsigned int FileManager::getDocIDFromName(const string& name)
{
    auto it = m_IDFileToPos.find(name);
    if( it != m_IDFileToPos.end() )
    {
        return it->second;
    }
    return UCHAR_MAX;
}


//unsigned int FileManager::getFrequencyInCollection(const string& token){

//    unsigned int tot = 0;
//    InvertedIndex *inv = InvertedIndex::getInstance();

//    vector<unsigned int> *list = inv->getPostingList(token);

//    if( list != NULL && list->size() > 0)
//    {

//        auto docID = list->begin();
//        if( list->size() % 2 == 1 )
//        {
//            tot += m_collection[ *docID ]->getFrequency(token);
//            docID++;
//        }
//        for( ; docID != list->end(); docID++)
//        {
//            tot += m_collection[ *docID ]->getFrequency(token);
//            docID++;
//            tot += m_collection[ *docID ]->getFrequency(token);
//        }

//    }

////// TODO CHECK FUNCTION TOO SLOW, CHECK ONLY THE FILES THAT ARE IN THE INVERTEX INDEX NOT ALL COLLECTION
////    for(vector<File*>::iterator it = m_collection.begin(); it != m_collection.end(); it++)
////    {
////        tot += (*it)->getFrequency(token);
////    }
//    return tot;
//}
