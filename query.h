#ifndef QUERY_H
#define QUERY_H

#include <string>
#include <vector>
#include <map>
#include "PreProcessor/tokenization.h"

#include "invertedindex.h"

using namespace std;

class Query
{
private:
    bool m_init;
    Tokenization m_tokens;

    vector<unsigned int> *m_unionPostingList;
    map<unsigned int, vector<string*> > *m_docIdPostingList;

public:
    Query(const string& query):
        m_init(false),
        m_unionPostingList(NULL)
    {
        m_tokens.tokenize(query.begin(), query.end());
    }
    ~Query()
    {
        if(isInit())
            this->unInit();
    }

    inline void init()
    {
        if(isInit())
            return;
        InvertedIndex *invM = InvertedIndex::getInstance();
        // get the union postingList
        m_unionPostingList = new vector<unsigned int>();
        invM->getUnionPostList( *m_tokens.getWords(), *m_unionPostingList);

        // construct structure for give a docID get the words
        m_docIdPostingList = new map<unsigned int, vector<string*> >();

        for( auto docID = m_unionPostingList->begin(); docID != m_unionPostingList->end(); docID++)
        {
            vector<string*> list(m_tokens.getWordsSize(),NULL);
            vector<string*>::iterator itList = list.begin();

            for( auto token = m_tokens.getWords()->begin(); token != m_tokens.getWords()->end(); ++token)
            {
                if( invM->isTokenInDoc(*token, *docID) )
                    *itList = &(*token);
                else
                    *itList = NULL;
                ++itList;
            }
            m_docIdPostingList->insert( pair<unsigned int, vector<string*> >(*docID, list));
        }

        m_init = true;
    }

    inline void unInit(){
        if(! m_init )
            return;
        delete m_unionPostingList;
        m_unionPostingList = NULL;
        delete m_docIdPostingList;
        m_docIdPostingList = NULL;
        m_init = false;
    }

    inline bool isInit() { return m_init;}

    inline vector<string> * getTerms() {
        return m_tokens.getWords();
    }

    inline vector<unsigned int> * getAllDocuments() {
        if(!isInit())
            init();
        return m_unionPostingList;
    }

    inline vector<string*> * getTermsIntersecDoc(unsigned int docID) {
        if(!isInit())
            init();

        auto it = m_docIdPostingList->find(docID);
        if( it != m_docIdPostingList->end() )
            return &it->second;
        return NULL;
    }

};

#endif // QUERY_H
