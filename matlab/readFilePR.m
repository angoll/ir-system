function [P, R] = readFilePR( filename)

    fileID = fopen(filename);
    
    P = [];%zeros(25);
    R = [];%zeros(25);
    
    if fileID == -1
        disp('Error at opening file');
        return;
    end

    n = 1;
    while ~feof(fileID)
        line = fgetl(fileID);
        data = sscanf(line, '%f,%f');
        P(n) = data(1);
        R(n) = data(2);
        n = n+1;
    end

    fclose(fileID);
    
end