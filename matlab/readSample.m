function readSample

	collection = [6,7];
	l = size(collection,2);
	Ptotal = [];
	Rtotal = [];
	hold off
	color = ['r','b','g'];
	for i = 1:l
		filename = ["Results_SmallColl_NoPorters/TFIDF/tfidf_PR_",int2str(collection(i)),".txt"];
		[P, R] = readFilePR(filename);
		P1 = P(1);
		R1 = R(1);
		for j = 2:size(R,2)
			if R(j)>R(j-1)
				P1 = [P1,P(j)];
				R1 = [R1,R(j)];
			end
		end
		g = plot(R1,P1,color(mod(i,3)));
		set(g, 'LineWidth', 5.0-i );
		xlabel('recall');
		ylabel('precision');
		axis([0,1,0,1]);
		hold on
	end
	hold off