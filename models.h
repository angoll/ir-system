#ifndef MODELS_H
#define MODELS_H

#include <string>
#include <vector>
#include <math.h>

#include "filemanager.h"
#include "invertedIndex.h"
#include "query.h"

using namespace std;

// global vars
extern FileManager *fileM;
extern InvertedIndex *invIndM;

// structure to save the scores and results
struct resultList {
  unsigned int docID;
  float score;
} ;


/*
 * Term frequency
 *
 * The term frequency, tf(t,d) of term t in document d is
 * defined as the number of times that t occurs in d
 */
inline float tf(const string& term, unsigned int docID)
{
    return FileManager::getInstance()->getTermFrequency(term, docID);
}

/*
 * idf
 *
 * Inverse document frequency
 *
 * idf(t) = log10(N/df(t))
 *
 * Where df(t) is the document frequency, ie. the number of
 * documents in the collection that the terms t occurs in.
 * and N is the nr of documents in the collection.
 */
inline float idf(const string& term){

    // nr of documents in the collection
    float N  = (float) fileM->getSize();
    float df = (float)invIndM->getPostingList(term)->size();

    return log10f( N / df);
}

/*
 * tfidf
 *
 * Term weighed inverse document frequency is the product of
 * a term's tf weight and its idf weight.
 *
 * weight(t,d) = (1 + log10(tf(t,d)) * idf(t))
 */
unsigned int tfidf(Query& query, vector<resultList>& result);

/*
*
* for sorting the strcutre result List
*
*/
bool sortList(resultList score1, resultList score2);

/*
 * bm25
 *
 * Implements the BM25 algorithm
 */
unsigned int bm25(Query& query, vector<resultList>& result, float k, float b);


/*
 * bm25partial
 *
 * returns the score of the fn
 */
float bm25partial(const string& token, unsigned int id, float k, float b);


void printResults(unsigned int queryID,const string& model, vector<resultList> &result);

void saveResults(const string& path,unsigned int queryID,const string& model, vector<resultList> &result);

pair<float,float> gridSearch(Query &query, unsigned int queryID, vector<unsigned int> &Y, float stepK, float stepB);

// I don't know where is this function :S
// vector<unsigned int> tf(Query query);


/*
 * tfidf
 *
 * Term weighed inverse document frequency is the product of
 * a term's tf weight and its idf weight.
 *
 * weight(t,d) = (1 + log10(tf(t,d)) * idf(t))
 */
vector<resultList> tfidf(Query query);


/*
 * precision and recall
 *
 */
void precisionRecall(const string &path, const string& model, unsigned int queryID, vector <resultList> &r, vector<unsigned int> &Y);

#endif // MODELS_H

