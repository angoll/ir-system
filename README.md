 IR System UvA 2013
====================

Understanfing of all things involved in a real information retrieval system.
Using the CSIRO document collection

Team:
-----
 * Camelia Simoiu
 * Hongpei Xu
 * Andreu Gonzalez Llinas


Setting up the project path:
-----------------------------

	Create in the project folder a file named config.h with the content:
	 	#ifndef CONFIG_H
	 	#define CONFIG_H
	 	#define PROJECT_PATH "" // folder where is located the folder Collection, the project path must end with /
	 	#endif 

 Collection:
-----------
	The search for the collection paths are based with the PROJECT_PATH
	· Full collection with stemming:
	 	PROJECT_PATH/Collection/BagOfWords.txt
	· Full collection without stemming: //Note: the full collection is not in the repo for size reasons
		PROJECT_PATH/corpus.body/
	· Small collection
	    PROJECT_PATH/CollectionOLD/

	· Querys and judgments
		PROJECT_PATH/Collection/topics-original.txt
		PROJECT_PATH/Collection/document-qrels.txt


Installation:
--------------
 · With Cmake:
	 - Mac Os / linux:

	     $> cd path_to_the_clone
	     $> cmake .
	     $> make

	 - Windows:

	     Use the graphical tool of Cmake, maybe the first try may generate an error, however for us, the error disappears on the second try

	     Use the cmd go to the build path and execute:
	     $> make

 · Using QT

 	Open QT, select the IRSystem.pro
 	Configure all the building system

System Requeriments:
---------------------

	For loading the full collection in RAM memory we need arround 2.5-3GB
	A C++ compiler that supports C++11

