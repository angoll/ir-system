#include "models.h"

#include <sstream>
#include <iostream>
#include <algorithm>

#include "File/file.h"
#include "File/bagofwords.h"
#include "adarankalgorithm.h"

#include <assert.h>

#include "qrelsmanager.h"
#include "other/filesystem.h"

#include <set>

FileManager *fileM = FileManager::getInstance();
InvertedIndex *invIndM = InvertedIndex::getInstance();


bool sortList(resultList list1, resultList list2)
{
    return list1.score > list2.score;
}

unsigned int tfidf(Query& query, vector<resultList>& result)
{
    resultList tmp;
    assert( result.size() == 0);
    assert( query.getAllDocuments() != NULL );

    set<unsigned int> test;
    pair<std::set<unsigned int>::iterator,bool> ret;

    // for each document in the common posting list
    for(vector<unsigned int>::iterator doc = query.getAllDocuments()->begin(); doc != query.getAllDocuments()->end(); ++doc)
    {
        ret = test.insert(*doc);
        assert( ret.second == true);
        tmp.docID = *doc;
        tmp.score = 0.f;
        assert( query.getTermsIntersecDoc(*doc) != NULL);
        // for each term in the query
        for(vector<string*>::iterator term = query.getTermsIntersecDoc(*doc)->begin(); term != query.getTermsIntersecDoc(*doc)->end(); ++term)
        {
            // if is NULL this term doesn't appear in the document
            if( *term != NULL)
            {
                // calculate tf.idf
                tmp.score += ( 1.f + log10f( tf(**term,*doc) ) ) * idf( **term);
            }
        }
        // push the results
        result.push_back( tmp );
    }

    sort(result.begin(),result.end(),sortList);
    return (unsigned int)result.size();
}


// passing the variable by reference: "vector<resultList>&"
// fn accepts the address of the variable
unsigned int bm25(Query& query, vector<resultList>& result, float k, float b)
{
    resultList tmp;

    // get average length of documents in the collection
    float L_ave = fileM->getAverageLength();

    // for each document in the common posting list
    for(vector<unsigned int>::iterator doc = query.getAllDocuments()->begin(); doc != query.getAllDocuments()->end(); ++doc)
    {
        tmp.docID = *doc;
        tmp.score = 0;

        // get length of the document
        float L_d = (float)fileM->getDocFromID(*doc)->getDocLength();
        float ldWeight = k * ( (1.f-b) + b * (L_d/L_ave));

        // for each term in the query
        for(vector<string*>::iterator term = query.getTermsIntersecDoc(*doc)->begin(); term != query.getTermsIntersecDoc(*doc)->end(); ++term)
        {
           // if is NULL this term doesn't appear in the document
            if( *term != NULL)
            {
                // tf for current query term & docID
                float termFreq = tf(**term, *doc);

                // calculate BM25 score
                tmp.score += idf(**term) * ( ((k + 1.f)*termFreq) / (termFreq + ldWeight) );
            }
        }
        result.push_back( tmp );

    }

    sort(result.begin(),result.end(),sortList);
    return (unsigned int)result.size();
}

float bm25partial(const string& token, unsigned int id, float k, float b)
{
//    float k = 1.2;
//    float b = 0.75;

    // get average length of documents in the collection
    float L_ave = FileManager::getInstance()->getAverageLength();

    // get length of the document
    float L_d = (float)fileM->getDocFromID(id)->getDocLength();
    float ldWeight = k * ( (1.f-b) + b * (L_d/L_ave));

    float termFreq = tf( token, id);

    float score = idf(token) * ( ((k + 1.f)*termFreq) / (termFreq + ldWeight) );

    return score ;




//    float score = 0;
//    // tf for current query term & docID
//    float termFreq = tf(token, id);

//    // if is NULL this term doesn't appear in the document
//    if( termFreq != 0)
//    {




//        // get average length of documents in the collection
//        float ldWeight = k * ( (1.f-b) + b * (L_d/L_ave));
//        float L_ave = fileM->getAverageLength();º

//        // calculate BM25 score
//        float dlWeight = k*( (1.f-b) + b * L_d/L_ave);
//        score =  idf(token) * (((k + 1.f)*termFreq )/(termFreq + dlWeight) );
//    }
//    return score;
}

//================================================================================
//================================================================================


void printResults(unsigned int queryID,const string& model, vector<resultList> &result)
{
    unsigned int count =0;
    FileManager *fileM = FileManager::getInstance();

    for(vector<resultList>::iterator i = result.begin(); i != result.end(); i++){
        count++;

        // format: <queryID> Q0 <documentID> <rank> <score> <runID>
        cout << queryID << " Q0 " << fileM->getDocFromID(i->docID)->getName() <<" " <<count <<" "  <<i->score <<" " <<model <<endl;
    }
}

void saveResults(const string& path,unsigned int queryID,const string& model, vector<resultList> &result)
{

    string fileName;
    fileName = path + model+ "_";
    ostringstream convert;
    convert<<queryID;
    fileName += convert.str();
    fileName += ".txt";


    DIR *dir = opendir(path.c_str());
    if(dir == NULL)
    {
        //cout<<"directory not exist creating"<<endl;
        Create_Directory( path.c_str());
    }

    ofstream file(fileName);
    if(!file.is_open())
    {
        cout<<"Error: Opening the file "<< fileName << endl;
        return;
    }

    FileManager *fileM = FileManager::getInstance();

    size_t count = 0;
    for(vector<resultList>::iterator i = result.begin(); i != result.end(); i++){
        count++;
        if( count == 200)
            break;
        // format: <queryID> Q0 <documentID> <rank> <score> <runID>
        file << queryID << " Q0 " << fileM->getDocFromID(i->docID)->getName() <<" " <<count <<" "  <<i->score <<" " <<model <<endl;
    }
    file.close();
}



// returns a series of okapi BM25 models with different parameters k, b
// giving a step for K, where 0 <= k <= 10
// and a step to search for b, where 0 <= b <= 1

pair<float,float> gridSearch(Query &query, unsigned int queryID, vector<unsigned int> &Y, float stepK, float stepB){

    (void)queryID;
    //At the extreme values of the coefficient  BM25 turns into ranking functions
    // known as BM11 (for b=1) and BM15 (for b=0)

    // in the absence of advanced optimization, usually chosen as
    // 1.2 <= k <= 2.0, and b = 0.75

    // set maximum value for k
    float limitK = 10;

    vector<resultList> result;
    float maxAP = 0;
    float maxK = 0;
    float maxB = 0;

    for (float b=0.f; b <= 1; b += stepB){

        for (float k=0.f; k <= limitK; k+=stepK){

            bm25(query, result, k, b);
            adaRank a;
            float APscore = a.E(result,Y);
            if ( APscore > maxAP ){
                maxAP = APscore;
                maxK =k;
                maxB = b;
            }

            // clean the vector result because if the elements are pushed back
            // the result vector will increase and every time you will have the results of
            // the current bm25() and the next bm25() and so on
            result.erase(result.begin(), result.end());
        }
    }
    //cout<< " _ "<<queryID << " bm25 "<< maxB << " "<< maxK << " "<< maxAP<<endl;
    return pair <float,float>(maxB,maxK);
}




void precisionRecall(const string &path, const string& model, unsigned int queryID, vector <resultList> &r, vector<unsigned int> &Y)
{

    string fileName;
    fileName = path + model+ "_";
    ostringstream convert;
    convert<<queryID;
    fileName += convert.str();
    fileName += ".txt";

    DIR *dir = opendir(path.c_str());
    if(dir == NULL)
        Create_Directory( path.c_str());

    ofstream file(fileName);
    if(!file.is_open())
    {
        cout<<"Error: Opening the file "<< fileName << endl;
        return;
    }


    // this is like a dictionary
    // each element is unique, a key
    set<unsigned int> setY(Y.begin(),Y.end());

    float R = 0;


    unsigned int max = ( r.size() > 25 ) ? 25 : r.size();

    for( unsigned int i = 0; i< max; ++i)
    {
        if (setY.count( r[i].docID )) {
            R += 1.f;
        }
        float precision = R / (float)(i+1);
        float recall = R / (float) max;
        file << precision << "," << recall << endl;

    }
    file.close();
}

