#ifndef QRELSMANAGER_H
#define QRELSMANAGER_H
#include <string>
#include <map>
#include <set>
#include "query.h"

using namespace std;

struct trainingSet {
    Query *query;
    vector<unsigned int> *ranks;
};

class QrelsManager
{
private:
    static QrelsManager *m_instance;

    map< unsigned int ,vector<unsigned int> > m_qrels; // contains a set with the docID of the rellevant documents for the query
    map<unsigned int, vector<unsigned int>>::iterator m_it;

    map<unsigned int, Query > m_querys;
    map<unsigned int, Query >::iterator m_qit;

    QrelsManager() {}

    bool initQRels(const string& name );
    bool initQuerys(const string& name);

public:
    static QrelsManager* getInstance();

    ~QrelsManager();

    inline bool init(string pathQRels, string pathQreys){
        return initQRels(pathQRels) &&  initQuerys(pathQreys);
    }

    Query * getQuery( unsigned int number);
    vector<unsigned int> * getJudgements( unsigned int number);

};

#endif // QRELSMANAGER_H
