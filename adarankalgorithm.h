#ifndef ADARANKALGORITHM_H
#define ADARANKALGORITHM_H

#include "query.h"
#include <vector>
#include "models.h"

using namespace std;

#define NFEATURES 7

class adaRank
{

    float  Fk[NFEATURES];
    float m_b,m_k;

    /*
     * Features :
     */
    unsigned int CalcFeatures(Query& query, vector< vector<resultList> >& result, bool sorted = true);

    unsigned int adaPi(Query &q, vector<resultList>& ranks, bool sorted = true);

public:

    /*
     * Measure the 'goodness' of ranking function
     * it's the E() function
     * This can bé MAP, NDCG, MRR, WTA ...
     */
    float E(vector<resultList>& actual, vector<unsigned int>& prediction);

    adaRank();

    inline void setB_K( float b, float k)
    { m_b = b; m_b = k;}

    void train(vector<Query*> &query, vector< vector<unsigned int>* > &Y);

    inline unsigned int test( Query& q, vector<resultList> &ranks)
    {
        return adaPi(q, ranks);
    }
};

#endif // ADARANKALGORITHM_H
