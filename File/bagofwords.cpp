#include "bagofwords.h"
#include <fstream>
#include <iostream>

BagOfWords::BagOfWords():
    m_totalWordCount(0)
{
}

BagOfWords::~BagOfWords()
{
    m_vocabulary.clear();
}

void BagOfWords::addWord(const string& token)
{

//    // v_1
//        m_it = m_vocabulary.find(token);

//        if( m_it != m_vocabulary.end())
//        {   // we find it
//            m_it->second++;
//        }else{
//             m_vocabulary.insert(PairStringUInt(token, 1));
//        }


//    // v_2
//      bool inserted = true;
//      pair<map<string,unsigned int>::iterator,bool>(m_it,inserted) = m_vocabulary.insert(PairStringUInt(token,1));
//      if(!inserted){
//          // insert fails
//          m_it->second++;
//      }

    // v_3
    m_it = m_vocabulary.lower_bound(token);

    if( m_it != m_vocabulary.end() && ! (m_vocabulary.key_comp()(token, m_it->first)))
    {   // Key already exist
        m_it->second ++;
    }
    else
    {   // the key does not exist in the map
        // use m_it (lb) as a hint to insert, so it can avoid another lookup
        m_vocabulary.insert(m_it, vocabulary_t::value_type(token, 1));
    }
}

unsigned int BagOfWords::getFrequency(string token)
{
    m_it = m_vocabulary.find(token);
    if( m_it != m_vocabulary.end())
    {   // we find it
        return m_it->second;
    }else{
        return 0;
    }

}

void BagOfWords::writeFile(ofstream& file)
{
//    // this only create a line the same lenght of the name
//    string bar(name.size(),'-');

//    // header of the file
//    file << bar << "\n";
//    file << name << "\n";
//    file << bar << "\n\n";

    // Write all the vocabulary in the file
    m_it = m_vocabulary.begin();
    if( m_vocabulary.size() % 2 == 1)
    {
        file << m_it->first << " : " << m_it->second << "\n";
        ++m_it;
    }
    for( ; m_it != m_vocabulary.end(); ++m_it )
    {
        file << m_it->first << " : " << m_it->second << "\n";
        ++m_it;
        file << m_it->first << " : " << m_it->second << "\n";
    }
//    for(m_it = m_vocabulary.begin(); m_it != m_vocabulary.end(); m_it++)
//    {
//        file << m_it->first << " : " << m_it->second << "\n";
//    }

    // Close the file
//    file.close();
}

void BagOfWords::countTotalWordCount()
{
    m_totalWordCount = 0;
    for(m_it = m_vocabulary.begin(); m_it != m_vocabulary.end(); m_it++)
        m_totalWordCount += m_it->second;
}

