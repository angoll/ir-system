#ifndef BAGOFWORDS_H
#define BAGOFWORDS_H
#include <map>
#include <string>

using namespace std;

typedef map<string, unsigned int> vocabulary_t;

class BagOfWords
{
private:
    typedef pair<string, unsigned int> PairStringUInt;

    vocabulary_t m_vocabulary;
    vocabulary_t::iterator m_it;

    unsigned int m_totalWordCount;

    // Allow to the InvertedIndex to access the private elements
    friend class InvertedIndex;

    /*
     * countTotalWordCount
     *
     * count the number of words in the file with repetition
     */
    void countTotalWordCount();

public:
    /*
     * BagOfWords
     *
     * Construct an object BagOfWords
     */
    BagOfWords();

    /*
     * ~BagOfWords
     *
     * Destructor of the BagOfWords release the memory
     */
    ~BagOfWords();

    /*
     * addWord
     * @param token: token or word string
     *
     * if the token don't exist is added to the vocabulary
     * if already exists we add one to his frequency
     */
    void addWord(const string& token);

    /*
     * addWord
     * @param token: token or word string
     * @param freq: frequency of the word
     *
     * For inizitialize with files typeBagOfWords
     */
    inline void addWord(const string& token, const unsigned int freq)
    { m_vocabulary.insert(PairStringUInt(token,freq));}

    /*
     * getSize
     *
     * Return the number of tokens we have in the BagOfWords
     */
    inline unsigned int getSize() { return (unsigned int) m_vocabulary.size();}

    /*
     * getFrequency
     * @param token: token or word string
     *
     * Returns the number of occurrences of the token in the file
     */
    unsigned int getFrequency( string token);

    /*
     * writeFile
     * @param fileName: path and name of the output file
     *
     * Write a file with the content of the vocabulary
     * word : frequency
     */
    //void writeFile(string fileName);
    void writeFile(ofstream &file);

    /*
     * getTotalWordCount
     *
     * Returns the total number of words in the file, with repetition;
     */
    inline unsigned int getTotalWordCount()
    {   if( m_totalWordCount == 0)
            countTotalWordCount();
        return m_totalWordCount;
    }



};
#endif // BAGOFWORDS_H
