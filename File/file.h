#ifndef FILE_H
#define FILE_H
#include <string>
#include <vector>
#include "bagofwords.h"
#include "../PreProcessor/PreProcessor.h"

using namespace std;

class File
{
private:
    string m_path;          // Path of the file
    string m_name;          // Name of the file
    string m_ext;           // Extension of the file
    BagOfWords m_bag;       // BagOfWords for this file

public:
    /*
     * File
     * @param fileName: path and name of the file
     *
     * Construct an object File that we want to Process
     */
    File(string fileName);

    /*
     * File
     * @param fileName: name of the file
     * @param Content: content of the file
     *
     * Construct an object File for the full collection
     */
    File(const string& fileName, const string& content);

    /*
     * File
     * @param fileName: name of the file
     *
     * Contruct an object File for the BagOfWords files
     */
    File(const string &fileName, void *);

    /*
     * ~File
     *
     * Destructor of the File release the memory
     */
    ~File();

    /*
     * process
     *
     * Process the file: 1. Create all the preprocessors
     *                   2. Read the file and make the tokenization
     *                      Tokenization: Split the text in words or tokens
     *                   3. For every token we apply the preprocessors
     *                   4. We add the words to the BagOfWords
     */
    void process();

    /*
     * isFileEmpty
     *
     * Return if the file after call process() is empty, so the
     * BagOfWords is empty, is no need to store the empty file
     */
    inline bool isFileEmpty() { return (m_bag.getSize() == 0); }

    /*
     * getName
     *
     * Return the name of the file
     */
    inline string getName() { return m_name;}

    /*
     * getFrequency
     * @param token: word or token to check in the BagOfWords
     *
     * Returns the frequency of a word from the bag of the file
     */
    unsigned int getFrequency(string token);

    /*
     * getBagOfWords
     *
     * Returns a pointer to the bagOfWords
     */
    inline BagOfWords * getBagOfWords() { return &m_bag;}

    /*
     * WriteFile
     * @param path: string that determine the path where the file
     *              will be stored.
     *
     * Create a file m_name_BagOfWords.txt containing all the words
     * and frequencies of the file
     */
    void writeFile(ofstream &file);

    /*
     * getDocLength
     *
     * returns the total word count in the document including repetitions
     */
    unsigned int getDocLength();
};

#endif // FILE_H
