#include "file.h"
#include <iostream>
#include <fstream>
#include <assert.h>

#include "../PreProcessor/PreProcessor.h"
#include "../PreProcessor/tokenization.h"

File::File(string fileName)
{
    unsigned foundLastBar = fileName.find_last_of("/\\");
    unsigned foundExt  = fileName.find_last_of(".");

    m_path = fileName.substr(0,foundLastBar+1);
    m_name = fileName.substr(foundLastBar+1, foundExt - foundLastBar -1);
    m_ext = fileName.substr(foundExt,fileName.size());
}

File::File(const string& fileName, const string& content):
    m_path(""),
    m_name(fileName),
    m_ext("")
{
    Tokenization to;
    to.tokenize(content.cbegin(), content.cend());

    vector<string>* _words = to.getWords();

    for( auto it = _words->cbegin(); it != _words->cend(); it++)
    {
        m_bag.addWord(*it);
    }
}

File::File(const string& fileName, void *):
    m_path(""),
    m_name(fileName),
    m_ext("")
{
}


File::~File()
{
}

void File::process()
{
    // Tokenitzation:
    Tokenization tokens;
    ifstream file;

    file.open( m_path+m_name+m_ext, ifstream::binary );

    if( !file.is_open() )
    {
        cout<< "Error: opening the file "<< m_path+m_name+m_ext <<endl;
        return;
    }

    file.seekg(0, file.end);
    size_t fileSize = file.tellg();
    file.seekg(0, file.beg);

    char *buffer = new char[ fileSize ];

    file.read(buffer, fileSize);

    if( !file )
        cout<< "error only "<<file.gcount()<< " could be read"<<endl;

    file.close();


    string str_buffer(buffer);
    delete [] buffer;
    tokens.tokenize(str_buffer.cbegin(), str_buffer.cend() );

    vector<string>* _words = tokens.getWords();

    //cout<<"words:"<<_words->size()<<endl;
    for( auto it = _words->cbegin(); it != _words->cend(); it++)
    {
        m_bag.addWord(*it);
    }

    return;
}


unsigned int File::getFrequency(string token)
{
    return m_bag.getFrequency(token);
}

void File::writeFile(ofstream& file)
{
    file<<"<DOC>\n<DOCNAME>\n";
    file<<m_name<<endl;
    file<<"</DOCNAME>\n<BAGOFWORDS>"<<endl;
    // call the BagOfWords to create the file
    m_bag.writeFile(file);
    file<<"</BAGOFWORDS>\n</DOC>"<<endl;
}

unsigned int File::getDocLength()
{
    return m_bag.getTotalWordCount();
}
