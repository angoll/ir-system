#include "qrelsmanager.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <climits>

#include "filemanager.h"
#include "other/common.h"

QrelsManager *QrelsManager::m_instance = NULL;

QrelsManager::~QrelsManager()
{
    m_instance = NULL;
    m_qrels.clear();
    m_querys.clear();
}
QrelsManager* QrelsManager::getInstance()
{
    if(m_instance == NULL)
        m_instance = new QrelsManager();
    return m_instance;
}

bool QrelsManager::initQRels(const string& name )
{
    ifstream _file;
    
    _file.open( name );
    if(!_file.is_open()){
        cout<<"Error: opening the file "<< name <<endl;
        return false;
    }

    string line;
    line.reserve(25);

    size_t currentQueryID= UCHAR_MAX;

    size_t queryID;
    size_t iteration;
    string docID;
    bool isRelevant;

    std::pair<std::map<unsigned int, vector<unsigned int> >::iterator,bool> ret;

    FileManager *fM = FileManager::getInstance();

    while( _file.good() )
    {
        getline(_file, line);

        if( ! line.empty() )
        {
            std::istringstream iss(line);
            if( !(iss>> queryID >> iteration >> docID >> isRelevant ) )
                   cout<<"Error reading qrels"<<endl;

            if( isRelevant)
            {
                unsigned int _dID = fM->getDocIDFromName(docID);
                if( _dID != UCHAR_MAX )
                {
                    if( currentQueryID == queryID)
                    {   // add elements to query
                        m_it->second.push_back(_dID);
                    }
                    else
                    {
                        vector<unsigned int> tmp;
                        tmp.push_back(_dID);
                        ret = m_qrels.insert(pair<unsigned int,vector<unsigned int> >(queryID, tmp));
                        m_it = ret.first;
                        currentQueryID = queryID;
                    }
                }
            }
        }
    }


    for( auto it = m_qrels.begin(); it != m_qrels.end(); ++it)
    {
        sort((it->second).begin(), (it->second).end());
    }



    cout<< " – QRELS loaded : " <<m_qrels.size()<<endl;
    return true;
}

bool QrelsManager::initQuerys(const string& name)
{
    ifstream _file;

    _file.open( name );
    if(!_file.is_open()){
        cout<<"Error: opening the file "<< name <<endl;
        return false;
    }

    _file.seekg(0, _file.end);
    size_t fileSize = _file.tellg();
    _file.seekg(0, _file.beg);

    char *buffer = new char[fileSize];

    _file.read(buffer,fileSize);

    if( !_file)
        cout<<" error only "<<_file.gcount()<<" could be read"<<endl;

    _file.close();

    bool tagTOP = false;
    bool tagNUM = false;
    bool tagQUERY = false;
    string tmp(""); tmp.reserve(10);
    string str_NUM(""); str_NUM.reserve(7);
    string str_QUERY(""); str_QUERY.reserve(200);

    size_t i=0;

    while( i < fileSize)
    {
//        cout<<"NUM:   "<<str_NUM<<endl;
//        cout<<"QUERY: "<<str_QUERY<<endl;
        if( buffer[i] == '<')
        {
            ++i;
            while( i < fileSize && buffer[i] != '>')
            {
                if( buffer[i] == '<')
                {   // incomplete tag or simple a <
                    if( tagTOP )
                    {
                        if( tagNUM)
                            str_NUM += "<"+ tmp;
                        else if( tagQUERY )
                            str_QUERY += "<"+ tmp;
                    }
                    tmp.erase(tmp.begin(), tmp.end());
                }
                else
                    tmp += buffer[i];

                ++i;
            }
            if( tmp == "top")
                tagTOP = true;
            else if( tmp == "/top")
            {
                ASSERT( !tagQUERY && !tagNUM, str_NUM);

                //cout<< str_NUM <<" -- "<< str_NUM.substr(3, 3) <<" -- ";
                std::istringstream reader(str_NUM.substr(3,3) );
                unsigned int numberID;
                reader >> numberID;

//                cout<< numberID <<" : "<<str_QUERY<<endl;
                Query q(str_QUERY);
                m_querys.insert(pair<unsigned int, Query>(numberID,q));

//                for(auto it = q.getTerms()->begin(); it != q.getTerms()->end(); it++)
//                {
//                    cout<<*it<<" ";
//                }
//                cout<<endl;

//                cout<< numberID <<endl;
//                cout<<" _ "<<str_QUERY<<endl;

                tagTOP = false ;
                str_NUM.erase(str_NUM.begin(), str_NUM.end());
                str_QUERY.erase(str_QUERY.begin(), str_QUERY.end());
            }
            else if( tmp == "num")
                tagNUM = true;
            else if( tmp == "/num")
                tagNUM = false;
            else if( tmp == "query")
                tagQUERY = true;
            else if( tmp == "/query")
                tagQUERY = false;
            else{
                // other tag
                if( tagTOP )
                {
                    if( tagNUM)
                        str_NUM += "<"+ tmp + ">";
                    else if( tagQUERY )
                        str_QUERY += "<"+ tmp + ">";
                }
            }
            tmp.erase(tmp.begin(), tmp.end());
        }
        else if( tagTOP )
        {
            if( tagNUM)
                str_NUM += buffer[i];
            else if( tagQUERY)
                str_QUERY += buffer[i];
        }

        ++i;
    }

    cout<< " – Querys loaded : " <<m_querys.size()<<endl;

    delete [] buffer;
    return true;
}


Query * QrelsManager::getQuery(unsigned int number)
{
    m_qit = m_querys.find( number );
    if( m_qit != m_querys.end() )
        return &(m_qit->second);
    return NULL;
}

vector<unsigned int> * QrelsManager::getJudgements(unsigned int number)
{
    m_it = m_qrels.find( number );
    if( m_it != m_qrels.end() )
        return &(m_it->second);
    return NULL;
}
