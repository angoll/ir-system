#ifndef InvertedIndex_H
#define InvertedIndex_H

/*
 * Singleton Class: means only one instance of this class will
 *                  exist during the execution of the program
 */
#include "File/file.h"
#include "File/bagofwords.h"
#include <map>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class InvertedIndex
{
private:
    static InvertedIndex *m_instance;

    struct content_t{
        unsigned int freqCollection;
        vector<unsigned int> * postingList;
    };

    typedef pair<string, content_t > PairStringVectorUInt;

    map<string, content_t > m_InvertedIndex;
    map<string, content_t >::iterator m_it;


    /*
     * InvertedIndex
     *
     * Private constructor: That's means only from in the class
     *                      can be created.
     */
    InvertedIndex();

public:
    /*
     * getInstance
     *
     * Returns the Instance of the object, if this instance is not
     * created, this is created.
     */
    inline static InvertedIndex* getInstance(){
        if(m_instance == NULL)
            m_instance = new InvertedIndex();
        return m_instance;
    }

    /*
     * ~InvertedIndex
     *
     * Destroy the object and release the memory
     */
    ~InvertedIndex();

    /*
     * addBagOfWords
     * @param id: the id of the file
     * @param BagOfWords: the bag of words of a file
     *
     * Add to the inverted index all the words in the bag of words
     * and join the duplicate words and add the file id to the vector
     */
    void addBagOfWords(unsigned int id, BagOfWords *word);

    /*
     * getFrequency
     * @param token: token or word string
     *
     * Returns the document frequency of a word
     */
    unsigned int getFrequency(const string& token);



    /*
     * getTotalWords
     *
     * Returns the number of words in the inverted index
     */
    inline unsigned int getTotalWords() { return (unsigned int)m_InvertedIndex.size(); }

    /*
     * writeFile
     * @param fileName: string with the name of the output file
     *
     * Write in a file all the inverted index: word : [ id, id, ..., id]
     */
    void writeFile(const string& fileName);

    /*
     * getPostingList
     *
     * Returns a vector of Documents ID's where this word appear
     */
    vector<unsigned int> * getPostingList(const string& token);

    /*
     * getIntersectPostList
     * @param tokens: list of all the tokens in the query
     * @param postList: list of all the documents that intersect with the query
     *
     * Returns the intersection of t € Q n D
     */
    unsigned int getIntersecPostList(vector<string>& tokens, vector<unsigned int> &postList);

    /*
     * getUnionPostList
     * @param tokens: list of all the tokens in the query
     * @param postList: list of all the documents related with the query
     *
     * Returns the union of t € Q u D
     */
    unsigned int getUnionPostList(vector<string>& tokens, vector<unsigned int> &postList);


    /*
     * isTokenInDoc
     * @param token: string of the word
     * @param docID
     *
     * return true if appears, false if not
     */
    inline bool isTokenInDoc(const string& token, unsigned int docID){
        vector<unsigned int> *list = getPostingList(token);
        if( list != NULL)
            return binary_search( list->begin(), list->end(), docID);
        return false;
    }

    /*
     * getFrequencyInCollection
     * @param token: string of the word
     *
     * Return the frequency of the word in the collection
     */
    inline unsigned int getFrequencyInCollection(const string &token)
    {
        m_it = m_InvertedIndex.find(token);
        if( m_it != m_InvertedIndex.end() )
            return m_it->second.freqCollection;
        return 0;
    }

};

#endif // InvertedIndex_H
