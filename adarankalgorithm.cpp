#include "adarankalgorithm.h"
#include <iostream>
#include <assert.h>
#include <set>

adaRank::adaRank()
{}

float adaRank::E(vector<resultList>& prediction, vector<unsigned int>& Y)
{
    float score = 0.f;
    float num_hits = 0.f;

    // crazy stufff
    // if prediction.size() > 200, maxSize = 200
    // otherwise keep maxSize = prediction.size()
    size_t maxSize = (prediction.size() >  200)? 200 : prediction.size();

    for(size_t i=0; i<maxSize; i++)
    {
        bool indicator1 = false;
        bool indicator2 = false;

        for(size_t j=0; j< Y.size(); j++)
        {
            if( Y[j] == prediction[i].docID )
            {
                indicator1 = true;
                break;
            }
        }

        for(size_t j=0; j< i; j++)
        {
            if(prediction[j].docID ==prediction[i].docID )
            {
                indicator2 = true;
                break;
            }
        }

        if( indicator1 && !indicator2 )
        {
            num_hits += 1.f;
            score +=  ( num_hits / (float)(i+1) );
        }
    }
    score = score / (float) Y.size();
    assert( score <= 1.f);
    return score;
}

//void adaRank::train( vector<Query*> &query, vector< vector<unsigned int>* > &Y)
//{
//    size_t M = query.size();
//    float m = (float) M;
//    float P[NFEATURES] = {1.f/m, 1.f/m, 1.f/m, 1.f/m, 1.f/m, 1.f/m, 1.f/m };
//    vector<float> measures(7*M, 0.f );

//    weight_Feature Fk;

//    for( size_t t=0; t < NFEATURES; ++t)
//    {
//        // calculate: E( π( qi, di, ht) , yi) once
//        for(size_t f=0; f<7; f++)
//        {
//            size_t rows = f*M;
//            for( size_t i = 0; i< M; i++)
//            {
//                vector<resultList> tmp;
//                if( adaPi( *(query[i]), f, tmp ) )
//                    measures.at( rows + i ) = E( tmp, *(Y[i]));
//                else
//                    cout<<"Error? list of ranks empty"<<endl;
//            }
//        }

//        // calculate the weak ranker
//        size_t hk = getWeakRanker( measures, &P[0] );

//        // calculate the weight, alpha
//        float num = 0.f, den = 0.f;
//        for(size_t i = 0; i<M; i++)
//        {
//            float tmp = measures[ hk*M + i];
//            num += P[i] * ( 1.f + tmp);
//            den += P[i] * ( 1.f - tmp);
//        }
//        float alpha = 0.5 * logf(num/den);

//        // calculate the F function
//        Fk.alpha = alpha;
//        Fk.f = hk;
//        m_rankingModel.push_back( Fk );

//        // reuse memory:
//        den = 0.0f;
//        // calculate P t+1 (i)
//        for(size_t i = 0; i< M; ++i )
//        {
//            vector<resultList> tmp;
//            if( adaPi( *(query[i]), m_rankingModel, tmp ) )
//            {
//                float aux = expf( - E( tmp, *Y[i]) );
//                measures[ i ] = aux; // re-use memory
//                den += aux;
//            }
//            else
//                cout<<"Error? list of ranks empty"<<endl;
//        }
//        for(size_t i =0; i< M ; ++i)
//        {
//            P[i] = measures[ i ] / den;
//        }
//    }

//}


void adaRank::train( vector<Query*> &query, vector< vector<unsigned int>* > &Y)
{
    size_t M = query.size();
    float m = (float) M;

    //float Fk[NFEATURES];    // weight for every feature
    float score[NFEATURES]; // acumulate score for feature
    for( size_t i=0; i< NFEATURES; ++i)
    {
        Fk[i] = 0.f;
        score[i] = 0.f;
    }

    //weight_Feature Fk; // TODO CHANGE

    // this part is for all the querys calculate the E( P(query, di,ht), yi)
    float *measures = new float[ 7*M ];
    float P[M];

    for(size_t i = 0; i< M; ++i)
    {
        P[i] = 1.f / m;

        vector< vector<resultList> > tmp(7);
        CalcFeatures( *(query[i]) , tmp );

        measures[ i*7 + 0 ] = E( tmp[0], *(Y[i]) );
        measures[ i*7 + 1 ] = E( tmp[1], *(Y[i]) );
        measures[ i*7 + 2 ] = E( tmp[2], *(Y[i]) );
        measures[ i*7 + 3 ] = E( tmp[3], *(Y[i]) );
        measures[ i*7 + 4 ] = E( tmp[4], *(Y[i]) );
        measures[ i*7 + 5 ] = E( tmp[5], *(Y[i]) );
        measures[ i*7 + 6 ] = E( tmp[6], *(Y[i]) );

        tmp[0].erase(tmp[0].begin(), tmp[0].end() );
        tmp[1].erase(tmp[1].begin(), tmp[1].end() );
        tmp[2].erase(tmp[2].begin(), tmp[2].end() );
        tmp[3].erase(tmp[3].begin(), tmp[3].end() );
        tmp[4].erase(tmp[4].begin(), tmp[4].end() );
        tmp[5].erase(tmp[5].begin(), tmp[5].end() );
        tmp[6].erase(tmp[6].begin(), tmp[6].end() );

    }

    for( size_t t=0; t < NFEATURES; ++t)
    {
        // calc weekRanker:
        score[0] = 0.f;
        score[1] = 0.f;
        score[2] = 0.f;
        score[3] = 0.f;
        score[4] = 0.f;
        score[5] = 0.f;
        score[6] = 0.f;

        for( size_t i =0; i< M; ++i)
        {
            score[0] += P[i] * measures[i*7 +0 ];
            score[1] += P[i] * measures[i*7 +1 ];
            score[2] += P[i] * measures[i*7 +2 ];
            score[3] += P[i] * measures[i*7 +3 ];
            score[4] += P[i] * measures[i*7 +4 ];
            score[5] += P[i] * measures[i*7 +5 ];
            score[6] += P[i] * measures[i*7 +6 ];
        }


        float maxScore = score[0];
        size_t hk = 0;
        for( int k =1; k< 7; ++k)
        {
            float _s = score[k];
            if( maxScore < _s )
            {
                maxScore = _s;
                hk = k;
            }
        }

        cout<<"weak ranker: " << hk <<endl;
        // calculate the weight, alpha
        float num = 0.f;
        float den = 0.f;
        for(size_t i = 0; i<M; i++)
        {
            float tmp = measures[ i*7 + hk ];
            num += P[i] * ( 1.f + tmp);
            den += P[i] * ( 1.f - tmp);
        }
        float alpha = 0.5 * logf(num/den);

        // calculate the F function
        Fk[ hk ] += alpha; // we acumulate the weigth for that concrete fetaure

        // reuse memory:
        den = 0.0f;
        float P_num[M];
        // calculate P t+1 (i)
        for(size_t i = 0; i< M; ++i )
        {
            vector<resultList> tmp;
            if( adaPi( *(query[i]), tmp ) )
            {
                float aux = expf( - E( tmp, *Y[i]) );
                P_num[i] = aux;
                den += aux;
            }
            else
                cout<<"Error? list of ranks empty"<<endl;
        }
        for(size_t i =0; i< M ; ++i)
        {
            P[i] = P_num[ i ] / den;
        }
    }

    delete[] measures;

    for( int i =0; i< 7; ++i)
    {
        if( Fk[i] != 0)
            cout<<"Select Feature: "<< i<<endl;
    }

    return;
}

unsigned int adaRank::CalcFeatures(Query& query, vector< vector<resultList> >& result, bool sorted)
{
    FileManager *fileM = FileManager::getInstance();
    resultList tmp[7];

    if( result.size() != 7)
    {
        cout<<"cant happen"<<endl;
        result.reserve(7);
    }

    result[0];
    result[1];
    result[2];
    result[3];
    result[4];
    result[5];
    result[6];



    // for each document in the common posting list
    for(vector<unsigned int>::iterator doc = query.getAllDocuments()->begin(); doc != query.getAllDocuments()->end(); ++doc)
    {
        unsigned int docID = *doc;
        tmp[0].docID = docID;
        tmp[0].score = 0;
        tmp[1].docID = docID;
        tmp[1].score = 0;
        tmp[2].docID = docID;
        tmp[2].score = 0;
        tmp[3].docID = docID;
        tmp[3].score = 0;
        tmp[4].docID = docID;
        tmp[4].score = 0;
        tmp[5].docID = docID;
        tmp[5].score = 0;
        tmp[6].docID = docID;
        tmp[6].score = 0;

        float dl = (float)fileM->getDocFromID(docID)->getDocLength();
        float cl = (float) fileM->getCollectionLength();

        // for each term in the query
        for(vector<string*>::iterator term = query.getTermsIntersecDoc(docID)->begin(); term != query.getTermsIntersecDoc(docID)->end(); ++term){
            // calculate feature
            if( *term != NULL)
            {
                float _tf = tf(**term, docID);
                float _idf = idf(**term);
                float cf = (float)fileM->getFrequencyInCollection(**term);
                tmp[0].score += logf(1.f + _tf );
                tmp[1].score += logf( ( cl / _tf ) + 1.f);
                tmp[2].score += logf( _idf );
                tmp[3].score += logf( (_tf / dl ) + 1.f);
                tmp[4].score += logf( (( _tf / dl) * _idf ) + 1.f );
                tmp[5].score += logf( ( (cf*cl)/(dl*cf) ) + 1.f);
                tmp[6].score += bm25partial(**term, docID,m_k,m_b) ;
            }
        }

        tmp[6].score = logf(tmp[6].score);
        result[0].push_back( tmp[0] );
        result[1].push_back( tmp[1] );
        result[2].push_back( tmp[2] );
        result[3].push_back( tmp[3] );
        result[4].push_back( tmp[4] );
        result[5].push_back( tmp[5] );
        result[6].push_back( tmp[6] );
    }

    if( sorted)
    {
        sort(result[0].begin(),result[0].end(),sortList);
        sort(result[1].begin(),result[1].end(),sortList);
        sort(result[2].begin(),result[2].end(),sortList);
        sort(result[3].begin(),result[3].end(),sortList);
        sort(result[4].begin(),result[4].end(),sortList);
        sort(result[5].begin(),result[5].end(),sortList);
        sort(result[6].begin(),result[6].end(),sortList);
    }

    return (unsigned int) result.size();
}



unsigned int adaRank::adaPi(Query& query, vector<resultList> &result, bool sorted)
{
    resultList finalTmp;
    float tmp[7];
    FileManager *fileM = FileManager::getInstance();

    // for each document in the common posting list
    for(vector<unsigned int>::iterator doc = query.getAllDocuments()->begin(); doc != query.getAllDocuments()->end(); ++doc)
    {
        unsigned int docID = *doc;
        tmp[0] = 0.f;
        tmp[1] = 0.f;
        tmp[2] = 0.f;
        tmp[3] = 0.f;
        tmp[4] = 0.f;
        tmp[5] = 0.f;
        tmp[6] = 0.f;

        float dl = (float)fileM->getDocFromID(docID)->getDocLength();
        float cl = (float) fileM->getCollectionLength();

        // for each term in the query
        for(vector<string*>::iterator term = query.getTermsIntersecDoc(docID)->begin(); term != query.getTermsIntersecDoc(docID)->end(); ++term)
        {
            // calculate feature
            if( *term != NULL)
            {
                float _tf = tf(**term, docID);
                float _idf = idf(**term);
                float cf = (float)fileM->getFrequencyInCollection(**term);
                tmp[0] += log(1.f + _tf );
                tmp[1] += logf( ( cl / _tf ) + 1.f);
                tmp[2] += logf( _idf );
                tmp[3] += logf( (_tf / dl ) + 1.f);
                tmp[4] += logf( (( _tf / dl) * _idf ) + 1.f );
                tmp[5] += logf( ( (cf*cl)/(dl*cf) ) + 1.f);
                tmp[6] += logf( bm25partial(**term, docID,m_k,m_b) );;
            }
        }


        finalTmp.docID = docID;
        finalTmp.score = 0.f;

        finalTmp.score += (Fk[0] * tmp[0]);
        finalTmp.score += (Fk[1] * tmp[1]);
        finalTmp.score += (Fk[2] * tmp[2]);
        finalTmp.score += (Fk[3] * tmp[3]);
        finalTmp.score += (Fk[4] * tmp[4]);
        finalTmp.score += (Fk[5] * tmp[5]);
        finalTmp.score += (Fk[6] * tmp[6]);

        result.push_back(finalTmp);
    }

    if( sorted)
    {
        sort(result.begin(),result.end(),sortList);
    }
    return (unsigned int) result.size();
}


