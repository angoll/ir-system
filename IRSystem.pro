TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += c++11#c++0x#c++11

QMAKE_CXXFLAGS += -O3 -fno-stack-protector

#QMAKE_CXXFLAGS += -m64

#QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp \
    invertedindex.cpp \
    filemanager.cpp \
    File/file.cpp \
    File/bagofwords.cpp \
	other/dirent_win.cpp \
	other/common.cpp \
	PreProcessor/tokenization.cpp \
	PreProcessor/PreProcessor.cpp \
	PreProcessor/porter2_stemmer.cpp \
	PreProcessor/porteralgorithm.cpp \
	PreProcessor/counter.cpp \
    models.cpp \
	qrelsmanager.cpp \
    adarankalgorithm.cpp

#    PreProcessor/email.cpp \
#    PreProcessor/domain.cpp

HEADERS += \
    invertedindex.h \
    filemanager.h \
    File/file.h \
    File/bagofwords.h \
	other/filesystem.h \
	other/dirent_win.h \
	other/common.h \
	config.h \
	PreProcessor/charByte_common.h \
	PreProcessor/htmlcodes.h \
	PreProcessor/tokenization.h \
	PreProcessor/PreProcessor.h \
	PreProcessor/porter2_stemmer.h \
	PreProcessor/porteralgorithm.h \
	PreProcessor/counter.h \
    models.h \
	query.h \
	qrelsmanager.h \
    PreProcessor/stopwords_list.h \
    adarankalgorithm.h

#    PreProcessor/email.h \
#    PreProcessor/domain.h
