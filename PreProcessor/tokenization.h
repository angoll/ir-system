#ifndef TOKENIZATION_H
#define TOKENIZATION_H

#include <iostream>
#include <vector>
#include <fstream>
#include <queue>

using namespace std;

class Tokenization
{
private:
    string::const_iterator m_current;
    queue<char> m_buffer;

    vector<string> m_words;

    inline char getNormalizedChar(string::const_iterator& it);
    inline char getHTMLCode(char c, string::const_iterator& it);


public:
    Tokenization() {}
    ~Tokenization() {}

    inline vector<string>* getWords(){ return &(this->m_words);}
    inline unsigned int getWordsSize(){ return (unsigned int)this->m_words.size();}

    void tokenize(string::const_iterator begin, string::const_iterator end);

};
#endif // TOKENIZATION_H
