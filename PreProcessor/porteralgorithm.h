#ifndef PORTERALGORITHM_H
#define PORTERALGORITHM_H
#include "PreProcessor.h"

class porterAlgorithm : public PreProcessor
{
public:
    porterAlgorithm() {}

    unsigned int Process(std::vector<std::string> &tokens);
};

#endif // PORTERALGORITHM_H
