#include "tokenization.h"
#include <iostream>
#include "charByte_common.h"
#include "htmlcodes.h"
#include "PreProcessor.h"
#include <stdlib.h>
#include "stopwords_list.h"

char Tokenization::getNormalizedChar(string::const_iterator& it)
{
    if( !m_buffer.empty())
    {
        char tmp = m_buffer.front();
        m_buffer.pop();
        return tmp;
    }

    char tmp = *it;
    ++it;

    if( tmp >= 0)
    {   // ASCII
        if( tmp >= 'A' && tmp <= 'Z')
            tmp += 32;
    }
    else
    {   // utf8
        string tmpUTF8;
        tmpUTF8 = tmp;
        bool error=false;
        int bytes = getUTF8_Bytes(tmp);
        switch( bytes )
        {
            case 4:
            {
                    tmp = *it;
                    if( tmp > 0){
                        error = true;
                        break;
                    }
                    tmpUTF8 += tmp;
                    ++it;
            }
            case 3:
            {
                    tmp = *it;
                    if( tmp > 0){
                        error = true;
                        break;
                    }
                    tmpUTF8 += tmp;
                    ++it;
            }
            case 2:
            {
                    tmp = *it;
                    if( tmp > 0){
                        error = true;
                        break;
                    }
                    tmpUTF8 += tmp;
                    ++it;
            }
            case 1: break;
            default:
                    break;
        }

        if( error )
        {
            //cout<<"UTF8 Symbol not well formed ("<<bytes<<")"<<endl;
            return ' ';
        }
        tmp = testGetCharFromUTF8(tmpUTF8);
    }
    return tmp;
}

char Tokenization::getHTMLCode(char c, string::const_iterator& it)
{
    char tmp = c;
    string tmp_str;
    tmp_str.reserve(10);
    tmp_str = c;
    size_t state = 0;
    bool stop = false;
    while(!stop)
    {
        tmp = getNormalizedChar(it);
        switch( state )
        {
        case 0:
        {
            tmp_str += tmp;
            if( tmp == '#')
                state = 1;
            else if ( tmp >= 'a' && tmp <= 'z')
                state = 2;
            else
                state = 3;
            break;
        }
        case 1:
        {
            tmp_str += tmp;
            if( tmp == 'x')
                state = 4;
            else if( tmp >= '0' && tmp <= '9')
                state = 5;
            else
                state = 3;
            break;
        }
        case 2:
        {   // &... String value the string can contain numbers - loop
            tmp_str += tmp;
            if( tmp == ';')
            {   // end state proccess the result for the Decimal
                stop = true;
                if( HTML_strCodeToString( tmp_str ) )
                    tmp = testGetCharFromUTF8(tmp_str);
                else
                    state = 3;
            }
            else if( ! ((tmp >= 'a' && tmp <= 'z') || ( tmp >= '0' && tmp <= '9')) )
                state = 3;
            break;
        }
        case 3:
        {   // error so exit, its not a htmlcode
            tmp_str += tmp;
            stop = true;
            for( auto it = tmp_str.begin()+1; it != tmp_str.end(); it++)
                m_buffer.push(*it);
            tmp_str.clear();
            break;
        }
        case 4:
        {   // &#x... Hexadecimal value start
            tmp_str += tmp;
            if( (tmp >= '0' && tmp <= '9') || (tmp >= 'a' && tmp <= 'z') )
                state = 6;
            else
                state = 3;
            break;
        }
        case 5:
        {   // &#... Decimal value - loop
            if( tmp == ';')
            {   // end state proccess the result for the Decimal
                stop = true;
                unsigned long codeNumber = strtoul( &(tmp_str.data()[2] ), NULL,10);
                HTML_numCodeToString(codeNumber, tmp_str);
                if( tmp_str.at(0) > 0)
                    tmp = tmp_str.at(0);
                else
                    tmp = testGetCharFromUTF8(tmp_str);
            }
            else if( ! (tmp >= '0' && tmp <= '9') )
                state = 3;
            tmp_str += tmp;
            break;
        }
        case 6:
        {   //&#x... Hexadecimal value - loop
            tmp_str += tmp;
            if( tmp == ';')
            {   // end state proccess the result for the Hexadecimal
                stop = true;
                unsigned long codeNumber = strtoul( &(tmp_str.data()[3] ), NULL,16);
                HTML_numCodeToString(codeNumber, tmp_str);
                if( tmp_str.at(0) > 0)
                    tmp = tmp_str.at(0);
                else
                    tmp = testGetCharFromUTF8(tmp_str);
            }
            else if( !((tmp >= '0' && tmp <= '9') || (tmp >= 'a' && tmp <= 'z')) )
                state = 3;
            break;
        }

        }
    }
    return tmp;
}

void Tokenization::tokenize(string::const_iterator begin, string::const_iterator end)
{
    assert(m_buffer.empty());

    if( begin == end)
        return;

    m_current = begin;
    char c;
    size_t state = 0;
    string word("");
    word.reserve(100);

    while(m_current < end)
    {
        //================================================================
        // 1 read the char and normalize, uper to lower or utf8 to ascci
        //================================================================
        c = getNormalizedChar(m_current);
        //================================================================
        // 2 check that c isn't an & and therefore a possible htmlcode
        //================================================================
        if( c == '&'){
            c = getHTMLCode(c, m_current);
        }
        //================================================================
        switch ( state )
        {
        case 0:
        {
            word += c;
            if( c >= '0' && c <='9' )
                state = 3;
            else if( c >= 'a' && c <= 'z' )
                state = 1;
            else
            {   // dont accept the word, restart
                state = 0;
                word.erase(word.begin(), word.end());
            }
            break;
        }
        case 1:
        {
            // final State if we go out(find that with the new char dont accept the word) we accept the word
            if( (c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') )
                state = 1;
            else if( c=='&' || c == '\'' || c == '@' || c == '.' || c == '-' || c == '_')
                state = 2;
            else
            {   // accept the word
                state = 0;
                if( !stopWordList.count(word) )
                    m_words.push_back(word);
                word.erase(word.begin(), word.end());
                break;
            }
            word += c;
            break;
        }
        case 2:
        {
            word += c;
            if( (c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') )
                state = 1;
            else
            {   // dont accept the word, restart
                state = 0;
                word.erase(word.begin(), word.end());
            }
            break;
        }
        case 3:
        {
            word += c;
            if( c >= '0' && c <='9' )
                state = 5;
            else if( c >= 'a' && c <= 'z' )
                state = 1;
            else if( c == '.' )
                state = 4;
            else if( c=='&' || c == '\'' || c == '@' || c == '-' || c == '_' )
                state = 2;
            else
            {   // dont accept the word, restart
                state = 0;
                word.erase(word.begin(), word.end());
            }
            break;
        }
        case 4:
        {   // point numbers.numbers
            word += c;
            if( c >= '0' && c <='9' )
                state = 4;
            else if( c >= 'a' && c <= 'z' )
                state = 1;
            else if( c=='&' || c == '\'' || c == '@' || c == '-' || c == '_' )
                state = 2;
            else
            {   // dont accept the word, restart
                state = 0;
                word.erase(word.begin(), word.end());
            }
            break;
        }
        case 5:
        {
            word += c;
            if( c >= '0' && c <='9' )
                state = 6;
            else if( c >= 'a' && c <= 'z' )
                state = 1;
            else if( c == '.' )
                state = 4;
            else if( c=='&' || c == '\'' || c == '@' || c == '-' || c == '_' )
                state = 2;
            else
            {   // dont accept the word, restart
                state = 0;
                word.erase(word.begin(), word.end());
            }
            break;
        }
        case 6:
        {
            word += c;
            if( c >= '0' && c <='9' )
                state = 7;
            else if( c >= 'a' && c <= 'z' )
                state = 1;
            else if( c == '.' )
                state = 4;
            else if( c=='&' || c == '\'' || c == '@' || c == '-' || c == '_' )
                state = 2;
            else
            {   // dont accept the word, restart
                state = 0;
                word.erase(word.begin(), word.end());
            }
            break;
        }
        case 7:
        {   // final State if we go out(find that with the new char dont accept the word) we accept the word
            if( c >= '0' && c <= '9' )
                state =8;
            else if( c >= 'a' && c <= 'z' )
                state = 1;
            else if(  c == '.')
                state = 4;
            else if( c == '\'' )
                state = 9;
            else if( c=='&' || c == '@' || c == '-' || c == '_')
                state = 2;
            else
            {   // accept the word
                state = 0;
                if( !stopWordList.count(word) )
                    m_words.push_back(word);
                word.erase(word.begin(), word.end());
                break; // not add this C
            }
            word += c;
            break;
        }
        case 8:
        {
            word += c;
            if( c >= '0' && c <='9' )
                state = 8;
            else if( c >= 'a' && c <= 'z' )
                state = 1;
            else if( c == '.' )
                state = 4;
            else if( c=='&' || c == '\'' || c == '@' || c == '-' || c == '_' )
                state = 2;
            else
            {   // dont accept the word, restart
                state = 0;
                word.erase(word.begin(), word.end());
            }
            break;
        }
        case 9:
        {   // final State if we go out(find that with the new char dont accept the word) we accept the word
            if( c >= '0' && c <= '9' )
                state =8;
            else if( c == 's')
            {
                state = 0;
                word.erase(word.end()-1);
                if( !stopWordList.count(word) )
                    m_words.push_back(word);
                word.erase(word.begin(), word.end());
                break; // not add this C
            }
            else if( c >= 'a' && c <= 'z' )
                state = 1;
            else
            {   // accept the word
                state = 0;
                word.erase(word.end()-1);
                if( !stopWordList.count(word) )
                    m_words.push_back(word);
                word.erase(word.begin(), word.end());
                break; // not add this C
            }
            word += c;
            break;
        }
        }
    }
    if( word.size() > 0 )
    {
        if( !stopWordList.count(word) )
            m_words.push_back(word);
    }
    // end of tokenization

    // start preprocessors:
    for( auto it = list_preprocessor->begin(); it != list_preprocessor->end(); it++)
    {
        size_t n_tokens = (*it)->Process(m_words);
        if( n_tokens == 0)
            return;
    }

//    auto it = m_words.begin();
//    while( it != m_words.end() )
//    {
//        if( stopWordList.count(*it) )
//            it = m_words.erase(it);
//        else
//            ++it;
//    }


    //cout<<test<<endl;
}
