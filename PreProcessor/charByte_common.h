#ifndef WORD_COMMON_H
#define WORD_COMMON_H
#include <string>
#include <map>
#include <set>
#include <assert.h>
#include <iostream>
// PREVIEW OF THE FUNCTIONS DECLARED:
// ===========================================================
// Common function to test an ASCII char
// ===========================================================
/// bool testCharIsLetter(char c);
/// bool testCharIsNumber(char c);
/// bool testCharIsSymbol(char c);
/// bool testCharIsSpecial(char c);
// ===========================================================
// Common function to test an UTF8 string/code
// ===========================================================
/// bool testUTF8IsLetter(std::string str);
/// bool testUTF8IsSymbol(std::string str);
// return true if it can be transformed to ascii
// return false if the content is ignored
/// bool testUTF8_toASCII(std::string& str);
// ===========================================================
// Common Tranform
// ===========================================================
// one symbol utf8 to lower case
inline std::string utf8_transformToLower( std::string& str);

// ===========================================================
// Common
// ===========================================================
// give the first char of an UTF8 returns the number of chars
// for coding the symbol
inline int getUTF8_Bytes(char c);


// ===========================================================
// Common UTF8 Data that we consider usefull
// ===========================================================
// List of all non-Ascii letters we consider, other discarted
static const std::set<std::string> utf8_data_letter = {
    "À","à","Á","á","Â","â","Ã","ã","Ä","ä","Å","å","Æ","æ",
    "Ç","ç","È","è","É","é","Ê","ê","Ë","ë","Ì","ì","Í","í",
    "Î","î","Ï","ï","Ð","ð","Ñ","ñ","Ò","ò","Ó","ó","Ô","ô",
    "Õ","õ","Ö","ö","Ø","ø","Ù","ù","Ú","ú","Û","û","Ü","ü",
    "Ý","ý","Þ","þ","Ā","ā","Ă","ă","Ą","ą","Ć","ć","Ĉ","ĉ",
    "Ċ","ċ","Č","č","Ď","ď","Đ","đ","Ē","ē","Ĕ","ĕ","Ė","ė",
    "Ę","ę","Ě","ě","Ĝ","ĝ","Ğ","ğ","Ġ","ġ","Ģ","ģ","Ĥ","ĥ",
    "Ħ","ħ","Ĩ","ĩ","Ī","ī","Ÿ","ÿ","Ĭ","ĭ","Į","į","İ","ı",
    "Ĳ","ĳ","Ĵ","ĵ","Ķ","ķ","ĸ","Ĺ","ĺ","Ļ","ļ","Ľ","ľ","Ŀ",
    "ŀ","Ł","ł","Ń","ń","Ņ","ņ","Ň","ň","ŉ","Ŋ","ŋ","Ō","ō",
    "Ŏ","ŏ","Ő","ő","Œ","œ","Ŕ","ŕ","Ŗ","ŗ","Ř","ř","Ś","ś",
    "Ŝ","ŝ","Ş","ş","Š","š","Ţ","ţ","Ť","ť","Ŧ","ŧ","Ũ","ũ",
    "Ū","ū","Ŭ","ŭ","Ů","ů","Ű","ű","Ų","ų","Ŵ","ŵ","Ŷ","ŷ",
    "Ź","ź","Ż","ż","Ž","ž","ß"
};

// List of all the lower letters
static const std::set< std::string > utf8_data_letters_Lower = {
    "à","æ","ì","ò","ù","ā","č","ę","ĥ","į","Ļ","Ň","œ","ş","ū",
    "ŷ","á","ç","í","ó","ú","ă","ď","ě","ħ","ı","Ľ","ŉ","ŕ","š",
    "ŭ","ź","ã","é","ï","õ","ü","ć","ē","ğ","ī","ĵ","Ł","ō","ř",
    "ť","ű","ž","ä","ê","ð","ö","ý","ĉ","ĕ","ġ","ÿ","ķ","Ń","ŏ",
    "ś","ŧ","ų","ß","å","ë","ñ","ø","þ","ċ","ė","ģ","ĭ","Ĺ","Ņ",
    "ő","ŝ","ũ","ŵ"
};

// Relation with capital letters to lower
static std::map<std::string, std::string> utf8_data_letters_toLower ={
    {"À","à"},{"Á","á"},{"Â","â"},{"Ã","ã"},{"Ä","ä"},{"Å","å"},
    {"Æ","æ"},{"Ç","ç"},{"È","è"},{"É","é"},{"Ê","ê"},{"Ë","ë"},
    {"Ì","ì"},{"Í","í"},{"Î","î"},{"Ï","ï"},{"Ð","ð"},{"Ñ","ñ"},
    {"Ò","ò"},{"Ó","ó"},{"Ô","ô"},{"Õ","õ"},{"Ö","ö"},{"Ø","ø"},
    {"Ù","ù"},{"Ú","ú"},{"Û","û"},{"Ü","ü"},{"Ý","ý"},{"Þ","þ"},
    {"Ā","ā"},{"Ă","ă"},{"Ą","ą"},{"Ć","ć"},{"Ĉ","ĉ"},{"Ċ","ċ"},
    {"Č","č"},{"Ď","ď"},{"Đ","đ"},{"Ē","ē"},{"Ĕ","ĕ"},{"Ė","ė"},
    {"Ę","ę"},{"Ě","ě"},{"Ĝ","ĝ"},{"Ğ","ğ"},{"Ġ","ġ"},{"Ģ","ģ"},
    {"Ĥ","ĥ"},{"Ħ","ħ"},{"Ĩ","ĩ"},{"Ī","ī"},{"Ÿ","ÿ"},{"Ĭ","ĭ"},
    {"Į","į"},{"İ","ı"},{"Ĳ","ĳ"},{"Ĵ","ĵ"},{"Ķ","ķ"},{"ĸ","Ĺ"},
    {"ĺ","Ļ"},{"ļ","Ľ"},{"ľ","Ŀ"},{"ŀ","Ł"},{"ł","Ń"},{"ń","Ņ"},
    {"ņ","Ň"},{"ň","ŉ"},{"Ŋ","ŋ"},{"Ō","ō"},{"Ŏ","ŏ"},{"Ő","ő"},
    {"Œ","œ"},{"Ŕ","ŕ"},{"Ŗ","ŗ"},{"Ř","ř"},{"Ś","ś"},{"Ŝ","ŝ"},
    {"Ş","ş"},{"Š","š"},{"Ţ","ţ"},{"Ť","ť"},{"Ŧ","ŧ"},{"Ũ","ũ"},
    {"Ū","ū"},{"Ŭ","ŭ"},{"Ů","ů"},{"Ű","ű"},{"Ų","ų"},{"Ŵ","ŵ"},
    {"Ŷ","ŷ"},{"Ź","ź"},{"Ż","ż"},{"Ž","ž"},{"ß","ß"}
};

// Relation with letters capital or lower to ASCII table
static const std::map<std::string, char> utf8_data_letters_ToAscii = {
    {"À",'a'},{"à",'a'},{"Á",'a'},{"á",'a'},{"Â",'a'},{"â",'a'},
    {"Ã",'a'},{"ã",'a'},{"Ä",'a'},{"ä",'a'},{"Å",'a'},{"å",'a'},
    {"Æ",'a'},{"æ",'a'},{"Ç",'c'},{"ç",'c'},{"È",'e'},{"è",'e'},
    {"É",'e'},{"é",'e'},{"Ê",'e'},{"ê",'e'},{"Ë",'e'},{"ë",'e'},
    {"Ì",'i'},{"ì",'i'},{"Í",'i'},{"í",'i'},{"Î",'i'},{"î",'i'},
    {"Ï",'i'},{"ï",'i'},{"Ð",'d'},{"ð",'o'},{"Ñ",'n'},{"ñ",'n'},
    {"Ò",'o'},{"ò",'o'},{"Ó",'o'},{"ó",'o'},{"Ô",'o'},{"ô",'o'},
    {"Õ",'o'},{"õ",'o'},{"Ö",'o'},{"ö",'o'},{"Ø",'o'},{"ø",'o'},
    {"Ù",'u'},{"ù",'u'},{"Ú",'u'},{"ú",'u'},{"Û",'u'},{"û",'u'},
    {"Ü",'u'},{"ü",'u'},{"Ý",'y'},{"ý",'y'},{"Þ",'p'},{"þ",'p'},
    {"Ā",'a'},{"ā",'a'},{"Ă",'a'},{"ă",'a'},{"Ą",'a'},{"ą",'a'},
    {"Ć",'c'},{"ć",'c'},{"Ĉ",'c'},{"ĉ",'c'},{"Ċ",'c'},{"ċ",'c'},
    {"Č",'c'},{"č",'c'},{"Ď",'d'},{"ď",'d'},{"Đ",'d'},{"đ",'d'},
    {"Ē",'e'},{"ē",'e'},{"Ĕ",'e'},{"ĕ",'e'},{"Ė",'e'},{"ė",'e'},
    {"Ę",'e'},{"ę",'e'},{"Ě",'e'},{"ě",'e'},{"Ĝ",'g'},{"ĝ",'g'},
    {"Ğ",'g'},{"ğ",'g'},{"Ġ",'g'},{"ġ",'g'},{"Ģ",'g'},{"ģ",'g'},
    {"Ĥ",'h'},{"ĥ",'h'},{"Ħ",'h'},{"ħ",'h'},{"Ĩ",'i'},{"ĩ",'i'},
    {"Ī",'i'},{"ī",'i'},{"Ÿ",'y'},{"ÿ",'y'},{"Ĭ",'i'},{"ĭ",'i'},
    {"Į",'i'},{"į",'i'},{"İ",'i'},{"ı",'i'},{"Ĳ",'i'},{"ĳ",'i'},
    {"Ĵ",'j'},{"ĵ",'j'},{"Ķ",'k'},{"ķ",'k'},{"ĸ",'k'},{"Ĺ",'l'},
    {"ĺ",'l'},{"Ļ",'l'},{"ļ",'l'},{"Ľ",'l'},{"ľ",'l'},{"Ŀ",'l'},
    {"ŀ",'l'},{"Ł",'l'},{"ł",'l'},{"Ń",'n'},{"ń",'n'},{"Ņ",'n'},
    {"ņ",'n'},{"Ň",'n'},{"ň",'n'},{"ŉ",'n'},{"Ŋ",'n'},{"ŋ",'n'},
    {"Ō",'o'},{"ō",'o'},{"Ŏ",'o'},{"ŏ",'o'},{"Ő",'o'},{"ő",'o'},
    {"Œ",'e'},{"œ",'e'},{"Ŕ",'r'},{"ŕ",'r'},{"Ŗ",'r'},{"ŗ",'r'},
    {"Ř",'r'},{"ř",'r'},{"Ś",'s'},{"ś",'s'},{"Ŝ",'s'},{"ŝ",'s'},
    {"Ş",'s'},{"ş",'s'},{"Š",'s'},{"š",'s'},{"Ţ",'t'},{"ţ",'t'},
    {"Ť",'t'},{"ť",'t'},{"Ŧ",'t'},{"ŧ",'t'},{"Ũ",'u'},{"ũ",'u'},
    {"Ū",'u'},{"ū",'u'},{"Ŭ",'u'},{"ŭ",'u'},{"Ů",'u'},{"ů",'u'},
    {"Ű",'u'},{"ű",'u'},{"Ų",'u'},{"ų",'u'},{"Ŵ",'w'},{"ŵ",'w'},
    {"Ŷ",'y'},{"ŷ",'y'},{"Ź",'z'},{"ź",'z'},{"Ż",'z'},{"ż",'z'},
    {"Ž",'z'},{"ž",'z'},{"ß",'b'},{"ß",'b'}
};

// List of all non-Ascii symbols we consider, other discarted
static const std::set<std::string> utf8_data_symbol = {
    "∀","″","´","&","∧","∠","'","≈","„","¦","•","∩","¸","¢",
    "ˆ","♣","≅","≥","©","↵","∪","¤","⇓","†","↓","°","♦","÷",
    "≡","∅"," "," "," "," ","€",">","⇔","↔","♥","½","¼","¾",
    "⁄","…","¡","ℑ","∞","∫","¿","∈","κ","⇐","λ","〈","«","←",
    "⌈","“","≤","⌊","∗","◊","\xE2\x80\x8E","‹","‘","<","¯","—",
    "·","−","∇","–","≠","∋","¬","∉","⊄","ν","‾","ω","ο",
    "⊕","∨","ª","º","⊗","¶","∂","‰","⊥","π","ϖ","±",
    "£","′","∏","∝","\"","⇒","√","〉","»","→","⌉","”","ℜ","®",
    "⌋","›","’","‚","⋅","§","\xC2\xAD","ς","∼","♠","⊂","⊆","∑",
    "⊃","¹","²","³","⊇","τ","∴","ϑ","˜","×","™","⇑","↑","¨",
    "ϒ","υ","℘","ξ","¥","\xE2\x80\x8F","\xE2\x80\x8D","\xE2\x80\x8C",

    "Α","Β","Χ","‡","Δ","Ε","Η","Γ","Ι","Κ","Λ","Μ","Ν","Ω",
    "Ο","Φ","Π","Ψ","Ρ","Σ","Þ","Τ","Θ","Υ","Ξ","ℵ","α","β","χ",
    "δ","ε","η","∃","ƒ","γ","ι","ζ","θ","σ","ρ","ψ","µ","μ","φ"
};
// transformation from utf8 to ascii common caracters others will be deleted
static const std::map< std::string, std::string > utf8_data_symbol_toAscii = {
    { "″", "\""},{"&", "&"},  { "´", "'"},{ "'", "'"},
    { "’", "'"}, { "‚", ","},  { "¸", "."},{ " ", " "},
    { " ", " "}, { " ", " "}, { " ", " "},{ "‘", "'"},
    { "—", "-"}, { "–", "-"}, { "−", "-"},{ "\"","\""},
    { "′", "'"}, { "“", "\""},{ "⁄", "/"},{ "”", "\""},
    { "@", "@"}
};

// here the symbols we want to keep in the words
static const std::set<std::string> data_valid_symbols = {
    ".","'","@","_", "-",
    "&","#",";","€","π","/","<",">"

};

static const std::set<char> data_valid_ASCII_symbols = {
    '&','\'','.','@','_','-','<','>','#',';'
};
// ===========================================================


// ===========================================================
// Common function to test an ASCII char
// ===========================================================
inline bool testCharIsLetter(char c)
{
   return ( c >= 'A' && c <= 'Z') || ( c >= 'a' && c <= 'z');
}

inline bool testCharIsNumber(char c)
{
   return ( c >= '0' && c <= '9');
}

inline bool testCharIsSymbol( char c )
{
    return ( c > 32 && c < 127 && !testCharIsLetter(c) && !testCharIsNumber(c));
}

inline bool testCharIsSpecial( char c)
{
    return ( c >= 0 && (c <= 32 || c == 127 ));
}

// ===========================================================
// Common function to test an UTF8 string
// ===========================================================
inline bool testUTF8IsLetter(const std::string str)
{
    return utf8_data_letter.count(str);
}

inline char testUTF8Letters_toASCII(const std::string str)
{
    auto it = utf8_data_letters_ToAscii.find(str);
    if( it != utf8_data_letters_ToAscii.cend() )
        return it->second;

    std::cout<< "UTF8_test: UTF8Letter not found: "<<str<<std::endl;
    return ' ';
}

inline bool testUTF8IsSymbol(std::string str)
{
    return utf8_data_symbol.count( str );
}

inline bool testUTF8_toASCII(std::string& str)
{
    std::map<std::string,std::string>::const_iterator it = utf8_data_symbol_toAscii.find( str );
    if( it != utf8_data_symbol_toAscii.cend() ){
        str = it->second;
        return true;    // symbol tranformed to ascii
    }
//    if( data_valid_symbols.count(str) )
//        return false;

//    str = { 0xFF,0xFF,0xFF,0xFF};
    return false;// ignored

}

inline char testGetCharFromUTF8( std::string& str)
{
    char tmp = ' ';
    if( testUTF8IsLetter(str) )
    {
        tmp = testUTF8Letters_toASCII( str );
    }
    else if( testUTF8IsSymbol(str) )
    {
        if( testUTF8_toASCII(str) )
        {
            tmp = str.at(0);
        }
        else
        {
            //std::cout<<"UTF8 Symbol not found: "<< str <<std::endl;
            tmp = ' ';
        }
    }
    else
    {
        //std::cout<<"UTF8 what is this??"<< str <<std::endl;
        tmp = ' ';
    }
    return tmp;
}

// ===========================================================
// Common utf8 functions:
// ===========================================================
inline std::string utf8_transformToLower( std::string& str)
{
    if( utf8_data_letters_Lower.count(str) )
        return str;

    std::map<std::string,std::string>::const_iterator it = utf8_data_letters_toLower.find(str);
    if( it != utf8_data_letters_toLower.cend() )
        return it->second;

    assert(true);
    return std::string("");
}


inline int getUTF8_Bytes(char c)
{
    if( (unsigned char)c <= 0x7F)
        return 1;
    if( (unsigned char)c <= 0xDF)
        return 2;
    if( (unsigned char)c <= 0xEF)
        return 3;
    if( (unsigned char)c <= 0xF7 )
        return 4;
    return -1;
}
// ===========================================================
#endif // WORD_COMMON_H
