#include "counter.h"
#include <iostream>

using namespace std;

Counter::Counter()
{
    m_countOf = 0;
    m_numtokens = 0;
}

unsigned int Counter::Process(vector<string> &tokens)
{
    // only lecture no need for copy
    for(vector<string>::const_iterator it = tokens.cbegin(); it != tokens.cend(); it++)
    {
        if( *it == "of")
                m_countOf ++;
        //++m_numtokens;
    }
    m_numtokens += tokens.size();
    return (unsigned int) tokens.size();
}

void Counter::ShowEnd()
{
    cout<<" – total count of the token \"of\": "<< m_countOf<<endl;
    cout<<" – total count of tokens with repetition: " <<m_numtokens<<endl;
}
