#ifndef HTMLCODES_H
#define HTMLCODES_H

//#include "word_t.h"
//#include "char_word.h"
#include <map>
#include <string>
#include <iostream>

using namespace std;

static const map<string, string> HTMLCodes_Data = {
    // LETERS
{ "AElig", "Æ" },{ "Aacute", "Á" },{ "Acirc", "Â" },{ "Agrave", "À" },
{ "Aring", "Å" },{ "Atilde", "Ã" },{ "Auml", "Ä" },{ "Ccedil", "Ç" },
{ "ETH", "Ð" },{ "Eacute", "É" },{ "Ecirc", "Ê" },{ "Egrave", "È" },
{ "Euml", "Ë" },{ "Iacute", "Í" },{ "Icirc", "Î" },{ "Igrave", "Ì" },
{ "Iuml", "Ï" },{ "Ntilde", "Ñ" },{ "OElig", "Œ" },{ "Oacute", "Ó" },
{ "Ocirc", "Ô" },{ "Ograve", "Ò" },{ "Oslash", "Ø" },{ "Otilde", "Õ" },
{ "Ouml", "Ö" },{ "Scaron", "Š" },{ "Uacute", "Ú" },{ "Ucirc", "Û" },
{ "Ugrave", "Ù" },{ "Uuml", "Ü" },{ "Yacute", "Ý" },{ "Yuml", "Ÿ" },
{ "Zeta", "Ζ" },
{ "aacute", "á" },{ "acirc", "â" },{ "aelig", "æ" },{ "agrave", "à" },
{ "aring", "å" },{ "atilde", "ã" },{ "auml", "ä" },{ "ccedil", "ç" },
{ "eacute", "é" },{ "ecirc", "ê" },{ "egrave", "è" },{ "eth", "ð" },
{ "euml", "ë" },{ "iacute", "í" },{ "icirc", "î" },{ "igrave", "ì" },
{ "iuml", "ï" },{ "yuml", "ÿ" },{ "uuml", "ü" },{ "yacute", "ý" },
{ "ucirc", "û" },{ "ugrave", "ù" },{ "thorn", "þ" },{ "uacute", "ú" },
{ "szlig", "ß" },{ "oslash", "ø" },{ "oacute", "ó" },{ "ocirc", "ô" },
{ "oelig", "œ" },{ "ograve", "ò" },{ "otilde", "õ" },{ "ntilde", "ñ" },
{ "ouml", "ö" },{ "scaron", "š" },

    // Math letters

{ "Alpha", "Α" },{ "Beta", "Β" },{ "Chi", "Χ" },{ "Dagger", "‡" },
{ "Delta", "Δ" },{ "Epsilon", "Ε" },{ "Eta", "Η" },{ "psi", "ψ" },
{ "Gamma", "Γ" },{ "Iota", "Ι" },{ "Kappa", "Κ" },{ "Lambda", "Λ" },
{ "Mu", "Μ" },{ "Nu", "Ν" },{ "Omega", "Ω" },{ "Omicron", "Ο" },
{ "Phi", "Φ" },{ "Pi", "Π" },{ "Psi", "Ψ" },{ "Rho", "Ρ" },
{ "Sigma", "Σ" },{ "THORN", "Þ" },{ "Tau", "Τ" },{ "Theta", "Θ" },
{ "Upsilon", "Υ" },{ "Xi", "Ξ" },{ "alefsym", "ℵ"},{ "alpha", "α" },
{ "beta", "β" },{ "chi", "χ" }, { "delta", "δ" }, { "epsilon", "ε" },
{ "eta", "η" },{ "exist", "∃" },{ "fnof", "ƒ" },{ "gamma", "γ" },
{ "iota", "ι" },{ "zeta", "ζ" },{ "theta", "θ" },{ "sigma", "σ" },
{ "rho", "ρ" },

    // Rest
{ "forall", "∀" },{ "Prime", "″" },{ "acute", "´" },{ "amp", "&" },
{ "and", "∧" },{ "ang", "∠" },{ "apos", "'" },{ "asymp", "≈" },
{ "bdquo", "„" },{ "brvbar", "¦" },{ "bull", "•" },{ "cap", "∩" },
{ "cedil", "¸" },{ "cent", "¢" },{ "circ", "ˆ" },{ "clubs", "♣" },
{ "cong", "≅" },{ "ge", "≥" },{ "empty", "∅" }, {"hearts", "♥" },
{ "copy", "©" },{ "crarr", "↵" },{ "cup", "∪" },{ "curren", "¤" },
{ "dArr", "⇓" },{ "dagger", "†" },{ "darr", "↓" },{ "deg", "°" },
{ "diams", "♦" },{ "divide", "÷" },{ "equiv", "≡" },
{ "emsp", " " },{ "thinsp", " " },{ "ensp", " " },{ "nbsp", " " },
{ "euro", "€" },{ "gt", ">" },{ "hArr", "⇔" },{ "harr", "↔" },
{ "frac12", "½" },{ "frac14", "¼" },{ "frac34", "¾" },{ "frasl", "⁄" },
{ "hellip", "…" },{ "iexcl", "¡" },{ "image", "ℑ" },{ "infin", "∞" },{ "int", "∫" },
{ "iquest", "¿" },{ "isin", "∈" },{ "kappa", "κ" },{ "lArr", "⇐" },
{ "laquo", "«" },{ "larr", "←" },{ "lceil", "⌈" },{ "ldquo", "“" },
    { "le", "≤" },{ "lfloor", "⌊" },{ "lowast", "∗" },{ "loz", "◊" },
{ "lrm", "\xE2\x80\x8E" },{ "lsaquo", "‹" },{ "lsquo", "‘" },{ "lt", "<" },
{ "macr", "¯" },{ "mdash", "—" },{ "micro", "µ" },{ "middot", "·" },
{ "minus", "−" },{ "mu", "μ" },{ "nabla", "∇" },{ "lambda", "λ" },{ "lang", "〈" },
{ "ndash", "–" },{ "ne", "≠" },{ "ni", "∋" },{ "not", "¬" },
{ "notin", "∉" },{ "nsub", "⊄" },{ "nu", "ν" },
{ "oline", "‾" },{ "omega", "ω" },{ "omicron", "ο" },{ "oplus", "⊕" },
{ "or", "∨" },{ "ordf", "ª" },{ "ordm", "º" },
{ "otimes", "⊗" },{ "para", "¶" },{ "part", "∂" },
{ "permil", "‰" },{ "perp", "⊥" },{ "phi", "φ" },{ "pi", "π" },
{ "piv", "ϖ" },{ "plusmn", "±" },{ "pound", "£" },{ "prime", "′" },
{ "prod", "∏" },{ "prop", "∝" },{ "quot", "\"" },
{ "rArr", "⇒" },{ "radic", "√" },{ "rang", "〉" },{ "raquo", "»" },
{ "rarr", "→" },{ "rceil", "⌉" },{ "rdquo", "”" },{ "real", "ℜ" },
{ "reg", "®" },{ "rfloor", "⌋" },
{ "rsaquo", "›" },{ "rsquo", "’" },{ "sbquo", "‚" },
{ "sdot", "⋅" },{ "sect", "§" },{ "shy", "\xC2\xAD" },
{ "sigmaf", "ς" },{ "sim", "∼" },{ "spades", "♠" },{ "sub", "⊂" },
{ "sube", "⊆" },{ "sum", "∑" },{ "sup", "⊃" },{ "sup1", "¹" },
{ "sup2", "²" },{ "sup3", "³" },{ "supe", "⊇" },
{ "tau", "τ" },{ "there4", "∴" },{ "thetasym", "ϑ" },
{ "tilde", "˜" },{ "times", "×" },
{ "trade", "™" },{ "uArr", "⇑" },{ "uarr", "↑" },
{ "uml", "¨" },{ "upsih", "ϒ" },{ "upsilon", "υ" },
{ "weierp", "℘" },{ "xi", "ξ" },{ "yen", "¥" },
 { "rlm", "\xE2\x80\x8F" },{ "zwj", "\xE2\x80\x8D" },{ "zwnj", "\xE2\x80\x8C" }
};


inline bool HTML_strCodeToString(string &code)
{
    // erase don't have & and ;
    //string s(code);

    assert( code.at(0) == '&' && code.at(code.length()-1) ==';');
    code.erase(code.begin());
    code.erase(code.end()-1);

    map<string,string>::const_iterator codeIt = HTMLCodes_Data.find(code);
    if( codeIt != HTMLCodes_Data.cend() ){
        // try to mantain the UpperCases
        code = codeIt->second;
    }else{
        // if not upercase try with lower
        for(size_t t=0; t<code.length(); t++)
        {    code.at(t) = tolower(code.at(t));}

        codeIt = HTMLCodes_Data.find( code );
        if( codeIt != HTMLCodes_Data.cend() ){
            code = codeIt->second;
        }else{
            return false;
            //cout << "symbol not found: "<< s <<endl;
        }
    }
    return true;
}

inline void HTML_numCodeToString(unsigned long codeNumber, string &str)
{
    //Numeric encoding: Decimal &#321; or hexadecimal &#x342;
    //bool hex = code.at(2) == 'x' || code.at(2) == 'X';
    //
    if( codeNumber == 160){
          str = " ";
    }else{
        //str = { 0xFF,0xFF,0xFF,0xFF};
        if( codeNumber <= 0x007Ful ){
            str = (unsigned char) codeNumber;
            return;
        }
        if( codeNumber <= 0x07FFul ) {
            str = (unsigned char)((6 << 5) | (codeNumber >> 6));
            str += (unsigned char)((2 << 6) | (codeNumber & 0x3F));
            return;
        }
        if( codeNumber <= 0xFFFFul ) {
            str = (unsigned char)(( 2 << 6) | ( codeNumber       & 0x3F));
            str += (unsigned char)(( 2 << 6) | ((codeNumber >> 6) & 0x3F));
            str += (unsigned char)((14 << 4) |  (codeNumber >> 12));
            return;
        }
        if( codeNumber <= 0x10FFFFul ) {
            str = (unsigned char)((30 << 3) |  (codeNumber >> 18));
            str += (unsigned char)(( 2 << 6) | ((codeNumber >> 12) & 0x3F));
            str += (unsigned char)(( 2 << 6) | ((codeNumber >>  6) & 0x3F));
            str += (unsigned char)(( 2 << 6) | ( codeNumber        & 0x3F));
            return;
        }
    }
}

#endif // HTMLCODES_H
