#include "porteralgorithm.h"
#include "porter2_stemmer.h"

#include "stopwords_list.h"

using namespace std;

unsigned int porterAlgorithm::Process(std::vector<std::string> &tokens)
{

    vector<string>::iterator newWord = tokens.begin();
    for( vector<string>::iterator it = tokens.begin(); it != tokens.end(); it++)
    {
        string tmp = Porter2Stemmer::stem( *it );
        if( !stopWordList.count(tmp) )
        {
            *newWord = tmp;
            ++newWord;
        }
    }

    tokens.erase(newWord,tokens.end());

    return (unsigned int) tokens.size();

}
