#ifndef COUNTER_H
#define COUNTER_H
#include "PreProcessor.h"

class Counter : public PreProcessor
{
private:
    unsigned int m_countOf;
    unsigned int m_numtokens;
public:
    Counter();
    unsigned int Process(std::vector<std::string> &tokens);

    void ShowEnd();
};

#endif // COUNTER_H
