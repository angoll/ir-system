#ifndef PREPROCESSOR_H
#define PREPROCESSOR_H

#include <string>
#include <vector>
#include <iostream>

/*
 * PreProcessorEnum
 *
 * DataType that determine all the preprocessors we can apply
 * TODO: Add all the preprocessors that we can use
 */


// remove the DOMAIN declared in <cmath>
#ifdef DOMAIN
#define TMP_DOMAIN DOMAIN
#undef DOMAIN
#endif

enum {
    None      = 0,
    STEMING   = 1 << 0,
    COUNTER   = 1 << 1,
    EMAIL     = 1 << 2,
    DOMAIN    = 1 << 3,
    other     = 1 << 4
};
typedef unsigned int PreProcessorEnum;

/*
 * Function to show the active Preprocessors
 */

static inline std::string showPreProcessors(PreProcessorEnum flags){
    std::string str("Flags Active\n");

    str += " Normalize (all lowerletters)\n";
    str += " HTMLCodes (all codes transformed to ASCII)\n";
    str += " Cleaning  (filter to accept words and split them)\n";

    if(flags & STEMING)
        str += " Steming\n";

    if(flags & COUNTER)
        str += " Counter\n";

    if(flags & EMAIL)
        str += " Email\n";

    if(flags & DOMAIN)
        str += " Domain\n";

    return str;
}

/*
 * Interface Class: this is only a class that obly all his
 *                  children to implement the function Process
 */

class PreProcessor
{
public:
    /*
     * Process
     * @param token:  word o token
     *
     * This is a pure virtual function means the code exist in the
     * child class, has to return an other token or empty token ""
     */
    virtual unsigned int Process(std::vector<std::string> &tokens)=0;

    /*
     * Destructor
     *
     * Virtual destructor to ensure that there aren't memory leaks
     */
    virtual ~PreProcessor() {}

    /*
     * ShowEnd
     *
     * Show an end function for the preprocessor
     */
    virtual void ShowEnd() {}
};

extern std::vector<PreProcessor*> *list_preprocessor;

/*
 * Includes of all preprocessors
 */

#include "porteralgorithm.h"
#include "counter.h"

//#include "domain.h"
//#include "email.h"

#ifdef TMP_DOMAIN
#undef DOMAIN
#define DOMAIN TMP_DOMAIN
#undef TMP_DOMAIN
#endif

#endif // PREPROCESSOR_H
